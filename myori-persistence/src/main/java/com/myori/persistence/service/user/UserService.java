package com.myori.persistence.service.user;

import com.myori.persistence.domain.session.User;
import com.myori.persistence.domain.session.Authority;
import com.myori.persistence.repository.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService, AuthenticationManager {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @PersistenceContext
    private final EntityManager entityManager;
    
    private final UserRepository userRepository;

    @Autowired
    public UserService(EntityManager entityManager, UserRepository userRepository) {
        this.entityManager = entityManager;
        this.userRepository = userRepository;
    }
        
    public Optional<User> getUserById(long id) {
        return userRepository.findById(id);
    }
    
    public Optional<User> getUserByName(String name) {
        return userRepository.findByName(name);
    }
    
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return (UserDetails) getUserByName(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username \"" + username + "\" not found"));
    }

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        return getUserByName(auth.getName())
                .filter(user -> user.getPassword().equals(auth.getCredentials()))
                .map(user -> new UsernamePasswordAuthenticationToken(auth.getName(), auth.getCredentials(), user.getGrantedAuthorities()))
                .orElseThrow(() -> new BadCredentialsException("Bad credential for user auth: " + auth.getName()));
    }

    public List<Authority> getAllAuthorities() {
        EntityManager em = getEntityManager();
        EntityTransaction et = em.getTransaction();
        et.begin();
        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<Authority> criteriaQuery = criteriaBuilder.createQuery(Authority.class);
            Root<Authority> root = criteriaQuery.from(Authority.class);
            criteriaQuery.select(root);
            return em.createQuery(criteriaQuery.select(root)).getResultList();
        } finally {
            et.commit();
            if (em.isOpen()) em.close();
        }
    }

    private EntityManager getEntityManager() {
        return entityManager.getEntityManagerFactory().createEntityManager();
    }
    
}
