package com.myori.persistence.service.order;

import com.myori.persistence.domain.order.Order;
import com.myori.persistence.repository.order.OrderRepository;
import com.myori.persistence.service.BasePersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends BasePersistenceService<Order> {
    
    private final OrderRepository repository;
    
    @Autowired
    public OrderService(OrderRepository repository) {
        this.repository = repository;
    }
    
    public Order createOrder() {
        return new Order();
    }
    
    @Override
    public Order save(Order order) {
        return super.save(order);
    }
    
}
