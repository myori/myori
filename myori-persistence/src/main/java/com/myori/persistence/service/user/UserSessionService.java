package com.myori.persistence.service.user;

import com.myori.persistence.domain.session.UserSession;
import com.myori.persistence.repository.session.UserSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserSessionService {
    
    private final UserSessionRepository repository;

    @Autowired
    public UserSessionService(UserSessionRepository repository) {
        this.repository = repository;
    }
    
    public Optional<UserSession> find(String userSessionId) {
        return repository.findById(userSessionId);
    }
}
