package com.myori.persistence.service;

import com.myori.persistence.repository.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.AbstractPersistable;

import java.io.Serializable;

public abstract class BasePersistenceService<T extends AbstractPersistable<? extends Serializable>> {
    
    @Autowired
    private BaseRepository<T, ? extends Serializable> baseRepository;
    
    protected T save(T entity) {
        return baseRepository.save(entity);
    }

}
