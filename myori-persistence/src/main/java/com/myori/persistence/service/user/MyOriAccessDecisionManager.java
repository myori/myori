package com.myori.persistence.service.user;

import com.myori.persistence.domain.session.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class MyOriAccessDecisionManager implements AccessDecisionManager {
    
    private final UserService userService;
    
    @Autowired
    public MyOriAccessDecisionManager(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        List<Authority> authorities = userService.getAllAuthorities();
        
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Authority.class.equals(clazz);
    }
    
}
