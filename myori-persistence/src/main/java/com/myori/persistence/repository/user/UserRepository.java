package com.myori.persistence.repository.user;

import com.myori.persistence.domain.session.User;
import com.myori.persistence.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User, Long> {
    
    Optional<User> findByName(String name);
    
}
