package com.myori.persistence.repository.module;

import com.myori.persistence.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModuleRepository extends BaseRepository<Module, Long> {
}
