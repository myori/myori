package com.myori.persistence.repository.city;

import com.myori.persistence.domain.city.City;
import com.myori.persistence.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends BaseRepository<City, Long> {
}
