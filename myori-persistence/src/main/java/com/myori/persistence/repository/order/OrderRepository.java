package com.myori.persistence.repository.order;

import com.myori.persistence.domain.order.Order;
import com.myori.persistence.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends BaseRepository<Order, String> {
}
