package com.myori.persistence.repository.session;

import com.myori.persistence.domain.session.UserSession;
import com.myori.persistence.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSessionRepository extends BaseRepository<UserSession, String> {
}
