package com.myori.persistence.domain.address;

import com.myori.persistence.domain.city.City;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;

@Entity
@Table(name = "address")
public class Address extends AbstractPersistable<Long> {
    @Version
    private long version;
/*
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "address",
            joinColumns = @JoinColumn(name = "address_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "city_id", referencedColumnName = "id")
    )
*/
    @OneToOne
    private City city;
    private String street;
    private String building;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }
}
