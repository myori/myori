package com.myori.persistence.domain.session;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity
public class Authority extends AbstractPersistable<Long> implements GrantedAuthority {
    
    @Version
    private long version;
    
    @Column(nullable = false, unique = true)
    private String authority;

    protected Authority() {}

    public Authority(String name) {
        this.authority = name;
    }
    
    @Override
    public String getAuthority() {
        return authority;
    }
}
