package com.myori.persistence.domain.common;

public enum State {
    
    NEW, CREATED, PREPARED, INPROCESS, INPROGRESS, COMPLETED
    
}
