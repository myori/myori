package com.myori.persistence.domain.order;

import com.myori.persistence.domain.common.State;
import com.myori.persistence.domain.common.Transition;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

@Embeddable
public class OrderStatusHistory extends Transition {
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private State fromState;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private State toState;

    protected OrderStatusHistory() {}

    public OrderStatusHistory(LocalDateTime transitionDateTime, State fromState, State toState) {
        super(transitionDateTime);
        this.fromState = fromState;
        this.toState = toState;
    }

    public State getFromState() {
        return fromState;
    }

    public void setFromState(State fromState) {
        this.fromState = fromState;
    }

    public State getToState() {
        return toState;
    }

    public void setToState(State toState) {
        this.toState = toState;
    }
}
