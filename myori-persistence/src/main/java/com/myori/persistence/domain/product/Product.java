package com.myori.persistence.domain.product;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;

@Entity
public class Product extends AbstractPersistable<Long> {
    
    @Version
    private long version;
    
    private String name;
    
    @ManyToOne(optional = false)
    private ProductType productType;
    
    @Enumerated(EnumType.STRING)
    private ProductStatus productStatus;

    protected Product() {}
    
    public Product(String name, ProductType productType, ProductStatus productStatus) {
        this.name = name;
        this.productType = productType;
        this.productStatus = productStatus;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public ProductStatus getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(ProductStatus productStatus) {
        this.productStatus = productStatus;
    }
}
