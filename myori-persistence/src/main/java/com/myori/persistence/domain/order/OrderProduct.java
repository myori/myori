package com.myori.persistence.domain.order;

import com.myori.persistence.domain.product.Product;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import java.util.Optional;

@Embeddable
public class OrderProduct {
    
    @OneToOne
    private Product product;

    @Column(name = "product_counts")
    private Integer count;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    void increment(int count) {
        this.count += count;
    }

    boolean filter(Product product) {
        return Optional.ofNullable(product).map(this.product::equals).orElse(false);
    }
}
