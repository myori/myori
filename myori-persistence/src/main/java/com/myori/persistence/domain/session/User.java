package com.myori.persistence.domain.session;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.Set;

@Entity
public class User extends AbstractPersistable<Long> {
    
    @Version
    private long version;
    
    private String name;
    
    private String password;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_granted_authorities",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_authority_id", referencedColumnName = "id")
    )
    private Set<Authority> grantedAuthorities;

    protected User() {}

    public User(String name, String password, Set<Authority> grantedAuthorities) {
        this.name = name;
        this.password = password;
        this.grantedAuthorities = grantedAuthorities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Authority> getGrantedAuthorities() {
        return grantedAuthorities;
    }

    public void setGrantedAuthorities(Set<Authority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }
}
