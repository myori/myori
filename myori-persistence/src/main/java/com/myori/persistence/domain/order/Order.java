package com.myori.persistence.domain.order;

import com.myori.persistence.domain.common.Comment;
import com.myori.persistence.domain.common.State;
import com.myori.persistence.domain.person.Person;
import com.myori.persistence.domain.product.Product;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

@Entity
@Table(name = "orders")
public class Order extends AbstractPersistable<Long> implements Serializable {

    @Version
    private long version;
    
    @ElementCollection
    @CollectionTable(name = "order_products")
    private Set<OrderProduct> orderProducts = new HashSet<>();
    
    @OneToOne
    private Person person;
    
    @Enumerated(value = EnumType.STRING)
    @Column(name = "order_state", nullable = false)
    private State state = State.NEW;
        
    private Comment comments;
    
    public Set<OrderProduct> getOrderProducts() {
        return Collections.unmodifiableSet(orderProducts);
    }

    public void setOrderProducts(Set<OrderProduct> orderProducts) {
        if (!State.NEW.equals(state)) {
            throw new IllegalStateException("Cant't set order products with order state: " + state);
        }
        
        this.orderProducts = orderProducts;
    }
    
    public void addProduct(Product product, int count) {
        if (!State.NEW.equals(state)) {
            throw new IllegalStateException("Cant't add product to order with state: " + state);
        }
        
        findOrCreate(product).increment(count);
    }
    
    private OrderProduct findOrCreate(Product product) {
        return orderProducts.stream()
                .filter(e -> e.filter(product))
                .findFirst()
                .orElseGet(createAndAdd(product));
    }
    
    private Supplier<OrderProduct> createAndAdd(Product product) {
        return () -> {
            OrderProduct newOrderProduct = new OrderProduct();
            newOrderProduct.setProduct(product);

            orderProducts.add(newOrderProduct);
            return newOrderProduct;
        };
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
    
    public Comment getComments() {
        return comments;
    }

    public void setComments(Comment comments) {
        this.comments = comments;
    }

}
