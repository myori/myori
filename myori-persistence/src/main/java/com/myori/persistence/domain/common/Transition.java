package com.myori.persistence.domain.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class Transition {
    
    @Column(updatable = false, nullable = false)
    private LocalDateTime transitionDateTime;

    protected Transition() {
        this(LocalDateTime.now());
    }

    public Transition(LocalDateTime transitionDateTime) {
        this.transitionDateTime = transitionDateTime;
    }

    public LocalDateTime getTransitionDateTime() {
        return transitionDateTime;
    }

    public void setTransitionDateTime(LocalDateTime transitionDateTime) {
        this.transitionDateTime = transitionDateTime;
    }
    
}
