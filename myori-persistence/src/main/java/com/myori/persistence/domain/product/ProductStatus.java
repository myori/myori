package com.myori.persistence.domain.product;

public enum ProductStatus {
    
    AVAILABLE, UNAVAILABLE, REMOVED_FROM_PRODUCTION
}
