package com.myori.persistence.domain.common;

import javax.persistence.Embeddable;

@Embeddable
public class Comment {
    
    private String comments;

    protected Comment() {}
    
    public Comment(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
