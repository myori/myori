package com.myori.persistence.domain.city;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.Version;

@Entity
public class City extends AbstractPersistable<Long> {
    
    @Version
    private long version;
    
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
