package com.myori.persistence.domain.module;

import com.myori.persistence.domain.session.Authority;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import java.util.Set;

@Entity
public class Module extends AbstractPersistable<Long> {
    
    @Version
    private long version;
    
    private String name;
    
    @OneToMany
    private Set<Authority> moduleAuthorities;

    protected Module() {}

    public Module(String name, Set<Authority> moduleAuthorities) {
        this.name = name;
        this.moduleAuthorities = moduleAuthorities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Authority> getModuleAuthorities() {
        return moduleAuthorities;
    }

    public void setModuleAuthorities(Set<Authority> moduleAuthorities) {
        this.moduleAuthorities = moduleAuthorities;
    }
}
