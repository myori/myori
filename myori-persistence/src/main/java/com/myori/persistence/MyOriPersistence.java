package com.myori.persistence;

import com.mongodb.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.mapping.event.LoggingEventListener;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories
@ComponentScan("com.myori.persistence")
@EntityScan
@PropertySource("classpath:application-db.properties")
public class MyOriPersistence {
    
    @Value("${spring.data.mongodb.host}")
    private String MONGO_HOST;
    
    @Value("${spring.data.mongodb.port}")
    private int MONGO_PORT;
    
    @Value("${spring.data.mongodb.database}")
    private String MYORI_DATABASE;

    private static final Logger LOGGER = LoggerFactory.getLogger(MyOriPersistence.class);
    
    public MyOriPersistence() {
        LOGGER.info("Create [PERSISTENCE CONTEXT CONFIGURATION]");
    }
    
    @Bean
    public LoggingEventListener loggingEventListener() {
        return new LoggingEventListener();
    }
    
    @Bean
    public MongoClient mongoClient() {
        return new MongoClient(MONGO_HOST, MONGO_PORT);
    }

    @Bean
    @Autowired
    public MongoDbFactory mongoDbFactory(MongoClient mongoClient) {
        return new SimpleMongoDbFactory(mongoClient, MYORI_DATABASE);
    }
    
    @Bean
    @Autowired
    public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory) {
        return new MongoTemplate(mongoDbFactory);
    }
    
}
