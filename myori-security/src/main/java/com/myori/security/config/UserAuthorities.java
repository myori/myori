package com.myori.security.config;

import org.springframework.security.core.GrantedAuthority;

public enum UserAuthorities implements GrantedAuthority {
    
    SUPERVISOR(Roles.SUPERVISOR),
    //ADMINISTRATOR(Roles.ADMINISTRATOR),
    USER(Roles.USER);
    //ANONYMOUS(Roles.ANONYMOUS);

    private final String authority;

    UserAuthorities(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}
