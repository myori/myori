package com.myori.security.config;

public interface Roles {

    String SUPERVISOR = "SUPERVISOR";
    String ADMINISTRATOR = "ADMINISTRATOR";
    String USER = "USER";
    String ANONYMOUS = "ANONYMOUS";

}
