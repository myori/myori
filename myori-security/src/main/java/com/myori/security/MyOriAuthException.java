package com.myori.security;


import org.springframework.security.core.AuthenticationException;

public class MyOriAuthException extends AuthenticationException {

    public MyOriAuthException(String msg, Throwable t) {
        super(msg, t);
    }

    public MyOriAuthException(String msg) {
        super(msg);
    }
}
