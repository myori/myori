package com.myori.security.providers.remote;

import org.springframework.security.authentication.AuthenticationProvider;

public interface RemoteAuthenticationProvider extends AuthenticationProvider {
}
