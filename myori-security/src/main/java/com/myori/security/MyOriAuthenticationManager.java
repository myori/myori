package com.myori.security;

import com.myori.security.config.UserAuthorities;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

class MyOriAuthenticationManager implements AuthenticationManager, Serializable {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return new MyOriAuth(findUser(authentication.getName()));
    }

    private UserDetails findUser(String name) {
        if ("prog1".equals(name)) {
            return new UserDetails() {
                
                @Override
                public Collection<? extends GrantedAuthority> getAuthorities() {
                    return Collections.singletonList(UserAuthorities.SUPERVISOR);
                }

                @Override
                public String getPassword() {
                    return "11111";
                }

                @Override
                public String getUsername() {
                    return "prog1";
                }

                @Override
                public boolean isAccountNonExpired() {
                    return false;
                }

                @Override
                public boolean isAccountNonLocked() {
                    return false;
                }

                @Override
                public boolean isCredentialsNonExpired() {
                    return true;
                }

                @Override
                public boolean isEnabled() {
                    return true;
                }
            };
        }

        return null;
    }

    private class MyOriAuth implements Authentication {

        private final UserDetails userDetails;

        MyOriAuth(UserDetails userDetails) {
            assert (userDetails != null);
            this.userDetails = userDetails;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return userDetails.getAuthorities();
        }

        @Override
        public Object getCredentials() {
            return userDetails;
        }

        @Override
        public Object getDetails() {
            return userDetails;
        }

        @Override
        public Object getPrincipal() {
            return userDetails;
        }

        @Override
        public boolean isAuthenticated() {
            return userDetails != null;
        }

        @Override
        public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
            if (!isAuthenticated) {
                throw new MyOriAuthException("User not authenticated");
            }
        }

        @Override
        public String getName() {
            return Optional.ofNullable(userDetails).map(UserDetails::getUsername).orElse("");
        }
    }
}
