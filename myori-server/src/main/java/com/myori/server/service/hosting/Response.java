package com.myori.server.service.hosting;

public interface Response<T> {

    T getData();

    void setData(T data);
}
