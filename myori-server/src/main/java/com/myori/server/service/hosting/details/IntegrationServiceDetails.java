package com.myori.server.service.hosting.details;

import com.myori.server.service.hosting.IntegrationService;
import com.myori.server.service.hosting.details.operation.ServiceOperation;

import java.util.List;

public abstract class IntegrationServiceDetails<T extends IntegrationService> {

    private final T serviceInstance;

    public IntegrationServiceDetails(T integrationService) {
        this.serviceInstance = integrationService;
    }

    protected List<ServiceOperation> getOperations() {
        return  null;
    }

    protected abstract int getOperationCount();

}
