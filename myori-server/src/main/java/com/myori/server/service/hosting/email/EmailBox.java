package com.myori.server.service.hosting.email;

import com.myori.server.service.hosting.email.Details.EmailBoxDetails;
import com.myori.server.service.hosting.email.Details.spam.EmailSpamPolicy;

public class EmailBox {

    private final EmailBoxDetails emailBoxDetails;

    private EmailSpamPolicy emailSpamPolicy;

    public EmailBox(EmailBoxDetails emailBoxDetails) {
        this.emailBoxDetails = emailBoxDetails;
    }

    public EmailBoxDetails getEmailBoxDetails() {
        return emailBoxDetails;
    }

    public EmailSpamPolicy getEmailSpamPolicy() {
        return emailSpamPolicy;
    }

    public void setEmailSpamPolicy(EmailSpamPolicy emailSpamPolicy) {
        this.emailSpamPolicy = emailSpamPolicy;
    }
}
