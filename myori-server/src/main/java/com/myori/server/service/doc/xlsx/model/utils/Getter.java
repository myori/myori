package com.myori.server.service.doc.xlsx.model.utils;

public interface Getter {

    <T, R, U> U getValue(T object, R method);

}
