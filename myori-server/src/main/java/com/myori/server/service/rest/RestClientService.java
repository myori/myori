package com.myori.server.service.rest;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

@Service
public class RestClientService {

    public Client createClient() {
        return ClientBuilder.newBuilder()
                .withConfig(buildConfig())
                .register(JacksonFeature.class)
                .build();
    }

    private ClientConfig buildConfig() {
        return new ClientConfig();
    }
    
}
