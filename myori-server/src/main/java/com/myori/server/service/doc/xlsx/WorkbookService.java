package com.myori.server.service.doc.xlsx;

import org.apache.poi.ss.usermodel.Row;

import java.io.InputStream;
import java.util.List;

public interface WorkbookService {

    List<Row> processWorkbook(InputStream input);

}
