package com.myori.server.service.hosting.email.Details;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EmailBoxDetails {

    @JsonProperty
    private int id;

    @JsonProperty
    private String name;

    @JsonProperty
    private String password;

    @JsonProperty
    private String type;

    @JsonProperty
    private String autospam;

    @JsonProperty
    private List<String> forward;

    @JsonProperty
    private Map<String, String> autoresponder;

    public EmailBoxDetails() {
        forward = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAutospam() {
        return autospam;
    }

    public void setAutospam(String autospam) {
        this.autospam = autospam;
    }

    public List<String> getForward() {
        return forward;
    }

    public void setForward(List<String> forward) {
        this.forward = forward;
    }

    public Map<String, String> getAutoresponder() {
        return autoresponder;
    }

    public void setAutoresponder(Map<String, String> autoresponder) {
        this.autoresponder = autoresponder;
    }
}
