package com.myori.server.service.auth;

import com.myori.server.service.rest.RestClientService;
import com.myori.server.service.rest.oriflame.OriflameRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

@Component
class OriflameAuthProvider implements AuthenticationProvider {

    @Value("${oriflame.config.base.url}")
    private String baseUrl;
    
    @Value("${oriflame.config.auth.path}")
    private String authPath;

    @Value("${oriflame.config.auth.query.param}")
    private String authQueryParam;

    @Value("${oriflame.config.auth.query.value}")
    private String authQueryValue;
    
    private final RestClientService restClientService;
    
    private final OriflameRestService oriflameRestService;

    @Autowired
    OriflameAuthProvider(RestClientService restClientService, OriflameRestService oriflameRestService) {
        this.restClientService = restClientService;
        this.oriflameRestService = oriflameRestService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Client client = restClientService.createClient();
        
        Response response = client.target(baseUrl)
                .path(authPath)
                .queryParam(authQueryParam, authQueryValue)
                .request()
                .get();
        
        oriflameRestService.registerClient(client);
        
        throw new UsernameNotFoundException("");
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return !UsernamePasswordAuthenticationToken.class.equals(clazz);
    }
    
}
