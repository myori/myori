package com.myori.server.service.hosting;

import com.myori.server.service.hosting.details.IntegrationServiceDetails;

public interface IntegrationService {

    IntegrationServiceDetails getDetails();

}
