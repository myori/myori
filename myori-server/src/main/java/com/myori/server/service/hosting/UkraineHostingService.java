package com.myori.server.service.hosting;

import com.myori.server.service.hosting.email.EmailBox;

import java.io.IOException;
import java.util.List;

public interface UkraineHostingService extends IntegrationService {

    List<EmailBox> getEmailBoxes() throws IOException;

}
