package com.myori.server.service.doc.xlsx;

import com.myori.server.service.doc.xlsx.model.utils.CellUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service(value = "xlsxService")
public class XLSXService implements WorkbookService {
    public static <T> Stream<T> streamFrom(Iterable<T> source) {
        return streamFrom(source, false);
    }
    public static <T> Stream<T> streamFrom(Iterable<T> source, boolean parallelProcessing) {
        return StreamSupport.stream(Optional.ofNullable(source).orElse(Collections.emptyList()).spliterator(), parallelProcessing);
    }

    private static Consumer<Row> echoAsCSV =
        row -> streamFrom(row).forEach(cell -> System.out.print("\"" + cell + "\"; "));

    private static Consumer<Row> printLineEnd = row -> System.out.println();

    @Override
    public List<Row> processWorkbook(InputStream input) {
        try {
            return processSheet(getSheet.apply(WorkbookFactory.create(input), 0));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    private static Predicate<Row> isDataRow = row -> !CellUtils.getCell.apply(row, 3).getCellTypeEnum().equals(CellType.STRING);
    private static Predicate<Row> notEmptyRow = row -> row.getLastCellNum() > 0 && !row.getCell(0).toString().contains("Период");

    public static BiFunction<Workbook, Integer, Sheet> getSheet = Workbook::getSheetAt;

    private static List<Row> processSheet(Sheet sheet) {
        List<Row> rows = new ArrayList<>();

        streamFrom(sheet).skip(7).peek(echoAsCSV.andThen(printLineEnd).andThen(rows::add)).allMatch(notEmptyRow);
        rows.remove(rows.size() - 1);
        return rows;
    }


}
