package com.myori.server.service.doc.xlsx.model.utils;

import com.myori.server.service.doc.xlsx.XLSXService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;


public interface CellUtils {

    BiFunction<Row, Integer, Cell> getCell = (row, index) -> row.getCell(index, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

    Function<Row, List<Cell>> getCellList = row -> XLSXService.streamFrom(row).collect(Collectors.toList());



}
