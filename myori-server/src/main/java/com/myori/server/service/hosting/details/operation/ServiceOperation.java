package com.myori.server.service.hosting.details.operation;

public interface ServiceOperation {

    String getName();

    String getInternalName();
}
