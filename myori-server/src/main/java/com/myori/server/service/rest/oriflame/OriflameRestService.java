package com.myori.server.service.rest.oriflame;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class OriflameRestService {
    
    @Value("${oriflame.config.base.url}")
    private String baseUrl;
    
    private final List<Client> clientsRegistry = new CopyOnWriteArrayList<>();
    
    public boolean registerClient(Client client) {
        return clientsRegistry.add(client);
    }
    
}
