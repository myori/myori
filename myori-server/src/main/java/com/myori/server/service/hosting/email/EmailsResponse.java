package com.myori.server.service.hosting.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.myori.server.service.hosting.Response;
import com.myori.server.service.hosting.email.Details.EmailBoxDetails;

import java.util.ArrayList;
import java.util.List;

public class EmailsResponse implements Response<List<EmailBoxDetails>> {

    @JsonProperty
    private String status;

    @JsonProperty
    private List<EmailBoxDetails> data;

    @JsonProperty
    private List<String> notes;

    public EmailsResponse() {
        data = new ArrayList<>();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public List<EmailBoxDetails> getData() {
        return data;
    }

    @Override
    public void setData(List<EmailBoxDetails> data) {
        this.data = data;
    }

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }
}
