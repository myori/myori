package com.myori.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "com.myori")
public class MyOriWebApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MyOriWebApplication.class, args);
    }

}
