package com.myori.server.rest.resource.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Component
public class AuthResource {
    
    @Autowired
    private HealthEndpoint healthEndpoint;
    
    @GET
    @Path("/health")
    public Object getHealth() {
        return healthEndpoint.health().getStatus();
    }
    
    @POST
    @Path("/login")
    public Response login(Credentials credential) {
        return Response.ok().build();
    }
    
    @POST
    @Path("/logout")
    public Response logout() {
        return Response.ok().build();
    }
    
}
