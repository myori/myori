package com.myori.server.rest.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UiResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(UiResource.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getRootResource() {
        LOGGER.debug("CALL GET ROOT");
        return "/index.html";
    }
}
