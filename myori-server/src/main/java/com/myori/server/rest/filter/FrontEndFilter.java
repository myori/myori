package com.myori.server.rest.filter;

import org.apache.commons.io.FilenameUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(value = "/*", asyncSupported = true)
public class FrontEndFilter implements Filter {

    private static final String UI_BOOTSTRAP_PATH = "/index.html";

    //Not works with tomcat
    //@Value("${web-headers.access-control-allow-origin}")
    private Boolean accessControlAllowOrigin = false;

    //Not works with tomcat
    //@Value("${spring.jersey.application-path}")
    private String apiPath = "/api";
    
    @Override
    public void init(FilterConfig filterConfig) {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //need for correct work without site covering

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String path = req.getRequestURI().substring(req.getContextPath().length()).replaceAll("[/]+$", "");

        // skip API requests
        if (path.startsWith(apiPath)) {
            chain.doFilter(request, response);
            return;
        }
        if (accessControlAllowOrigin) {
            // allow CORS
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
            res.setHeader("Access-Control-Max-Age", "3600");
            res.setHeader("Access-Control-Allow-Headers", "x-requested-with ,S2-User-Language , content-type, X-Auth-Token");
            res.setHeader("Access-Control-Expose-Headers", "S2-User-Language, X-Auth-Token");
        }

        if (StringUtils.isEmpty(FilenameUtils.getExtension(path))) {
            RequestDispatcher x = req.getRequestDispatcher(UI_BOOTSTRAP_PATH);
            x.forward(req, res);
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {}

}
