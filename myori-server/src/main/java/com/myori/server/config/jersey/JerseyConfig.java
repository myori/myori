package com.myori.server.config.jersey;

import com.myori.server.config.DefaultRestProperties;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/api/rest")
@EnableConfigurationProperties(DefaultRestProperties.class)
public class JerseyConfig extends ResourceConfig {

    private final DefaultRestProperties properties;

    @Autowired
    public JerseyConfig(DefaultRestProperties properties) {
        super();
        this.properties = properties;
        initProperties();

        register(RequestContextFilter.class);
        packages("com.myori.server.rest", "com.myori.integration.rest");
    }

    private void initProperties() {
        super.property("path", properties.getRootPath());
    }

}
