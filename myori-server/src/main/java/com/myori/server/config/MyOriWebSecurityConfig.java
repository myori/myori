package com.myori.server.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
@EnableConfigurationProperties(DefaultWebProperties.class)
@EnableWebSecurity
public class MyOriWebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ENCODING = "UTF-8";
    private static final String SESSION_ID_PROP_NAME = "JSESSIONID";

    private final DefaultWebProperties defaultWebProperties;

    private final Logger LOGGER = LoggerFactory.getLogger(MyOriWebSecurityConfig.class);

    @Autowired
    public MyOriWebSecurityConfig(DefaultWebProperties defaultWebProperties) {
        super();
        LOGGER.info("Create MyOri-Server Web Security context configuration");
        this.defaultWebProperties = defaultWebProperties;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        addEncodingFilter(http);

        http.authorizeRequests()
                .antMatchers("/css/**", "/img/**").permitAll()
                .antMatchers(defaultWebProperties.getStaticResources()).permitAll()
                .antMatchers(defaultWebProperties.getLogin()).permitAll()
                .antMatchers("/app/view/**").permitAll()
                .antMatchers("/**/api/rest/**").authenticated()
                .and().csrf().disable();
                    //.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }

    private void addEncodingFilter(HttpSecurity http) {
        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
        encodingFilter.setEncoding(ENCODING);
        encodingFilter.setForceEncoding(true);

        http.addFilterBefore(encodingFilter, CsrfFilter.class);
        LOGGER.debug("Encoding Filter added to filter chain.");
    }

}
