package com.myori.server.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "myori.web.config.default.url")
public class DefaultWebProperties {
    
    private String auth;
    private String login;
    private String logout;
    private String error;
    private String success;
    private String[] staticResources;
    private String secure;
    
    private String loginProp;
    private String passwordProp;

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogout() {
        return logout;
    }

    public void setLogout(String logout) {
        this.logout = logout;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String[] getStaticResources() {
        return staticResources;
    }

    public void setStaticResources(String[] staticResources) {
        this.staticResources = staticResources;
    }

    public String getSecure() {
        return secure;
    }

    public void setSecure(String secure) {
        this.secure = secure;
    }

    public String getLoginProp() {
        return loginProp;
    }

    public void setLoginProp(String loginProp) {
        this.loginProp = loginProp;
    }

    public String getPasswordProp() {
        return passwordProp;
    }

    public void setPasswordProp(String passwordProp) {
        this.passwordProp = passwordProp;
    }
}
