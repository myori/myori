package com.myori.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import java.util.Properties;

@Configuration
public class WebConfig {
    
    @Value("${spring.messages.basename}")
    private String basename;

    
    public MessageSource msgs() {
        Properties prop = new Properties();
        prop.put(basename + "_en", "UTF-8");
        prop.put(basename + "_ru", "UTF-8");
        prop.put(basename + "_uk", "UTF-8");


        ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();
        source.setBasename(basename);
        source.setDefaultEncoding("UTF-8");
        source.setFileEncodings(prop);
        
        return source; 
    }

    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludeQueryString(true);
        loggingFilter.setIncludePayload(true);
        return loggingFilter;
    }
}
