package com.myori.integration.instagram.api.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.myori.integration.instagram.api.InstagramProfile;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown=true)
abstract class CaptionMixin {
    @JsonCreator
    CaptionMixin(
            @JsonProperty("id") long id,
            @JsonProperty("created_time") @JsonDeserialize(using=InstagramDateDeserializer.class) Date createdTime,
            @JsonProperty("text") String text,
            @JsonProperty("from") InstagramProfile from) {}
}
