package com.myori.integration.instagram.api.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.myori.integration.instagram.api.Relationship;

import java.io.IOException;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RelationshipMixin {
    @JsonCreator
    RelationshipMixin(
            @JsonProperty("outgoing_status") @JsonDeserialize(using=OutgoingDeserializer.class) Relationship.OutgoingStatus outgoingStatus,
            @JsonProperty("incoming_status") @JsonDeserialize(using=IncomingDeserializer.class) Relationship.IncomingStatus incomingStatus) {}

    private static class OutgoingDeserializer extends JsonDeserializer<Relationship.OutgoingStatus> {
        @Override
        public Relationship.OutgoingStatus deserialize(JsonParser jp, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            return Relationship.OutgoingStatus.valueOf(jp.getText().toUpperCase());
        }
    }

    private static class IncomingDeserializer extends JsonDeserializer<Relationship.IncomingStatus> {
        @Override
        public Relationship.IncomingStatus deserialize(JsonParser jp, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            return Relationship.IncomingStatus.valueOf(jp.getText().toUpperCase());
        }
    }
}
