package com.myori.integration.instagram.api.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.myori.integration.instagram.api.Relationship;

import java.io.IOException;

public class RelationshipContainerDeserializer extends AbstractInstagramDeserializer<RelationshipContainer> {

    @Override public RelationshipContainer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        return deserializeResponseObject(jp, RelationshipContainer.class, Relationship.class);
    }
}
