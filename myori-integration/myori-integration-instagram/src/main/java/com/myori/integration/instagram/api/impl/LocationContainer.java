package com.myori.integration.instagram.api.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.myori.integration.instagram.api.Location;

import java.io.IOException;

@JsonDeserialize(using=LocationContainer.LocationContainerDeseriazlier.class)
public class LocationContainer extends AbstractInstagramResponseContainer<Location> {

    public static class LocationContainerDeseriazlier extends AbstractInstagramDeserializer<LocationContainer> {
        @Override
        public LocationContainer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            return deserializeResponseObject(jp, LocationContainer.class, Location.class);
        }
    }

    public LocationContainer(Location location) {
        super(location);
    }
}
