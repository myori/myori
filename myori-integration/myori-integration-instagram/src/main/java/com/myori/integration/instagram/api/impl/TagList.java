package com.myori.integration.instagram.api.impl;

import com.myori.integration.instagram.api.Tag;

import java.util.List;

public class TagList {

	private List<Tag> list;

	public TagList(List<Tag> list) {
		this.list = list;
	}

	public List<Tag> getList() {
		return list;
	}
}
