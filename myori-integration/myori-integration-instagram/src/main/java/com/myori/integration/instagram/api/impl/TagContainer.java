package com.myori.integration.instagram.api.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.myori.integration.instagram.api.Tag;

import java.io.IOException;

@JsonDeserialize(using=TagContainer.TagContainerDeserializer.class)
public class TagContainer extends AbstractInstagramResponseContainer<Tag> {

    public TagContainer(Tag tag) {
        super(tag);
    }

    public static class TagContainerDeserializer extends AbstractInstagramDeserializer<TagContainer> {

        @Override
        public TagContainer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
            return deserializeResponseObject(jp, TagContainer.class, Tag.class);
        }

    }

}
