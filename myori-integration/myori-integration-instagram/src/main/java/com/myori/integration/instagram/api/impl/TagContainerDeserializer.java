package com.myori.integration.instagram.api.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.myori.integration.instagram.api.Tag;

import java.io.IOException;

public class TagContainerDeserializer extends AbstractInstagramDeserializer<TagContainer> {

    @Override
    public TagContainer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        return deserializeResponseObject(jp, TagContainer.class, Tag.class);
    }

}
