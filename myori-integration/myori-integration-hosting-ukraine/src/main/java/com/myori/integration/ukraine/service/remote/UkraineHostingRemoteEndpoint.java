package com.myori.integration.ukraine.service.remote;

import com.myori.integration.core.service.transport.context.payload.remote.RestEndpoint;
import com.myori.integration.ukraine.model.EmailsResponse;
import com.myori.integration.ukraine.model.email.EmailBox;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class UkraineHostingRemoteEndpoint extends RestEndpoint {

    private static final String USER_NAME_PARAM = "auth_login";
    private static final String TOKEN_PARAM = "auth_token";
    private static final String ACCOUNT_PARAM = "account";
    private static final String DOMAIN_PARAM = "domain";
    private static final String CLASS_PARAM = "class";
    private static final String EMAIL_CLASS = "hosting_mailbox";
    private static final String METHOD_PARAM = "method";
    private static final String INFO_METHOD = "info";
    private final String login;
    private final String token;
    private final String account;
    private final String domain;

    public UkraineHostingRemoteEndpoint(String serviceUrl, String userName, String token, String account, String domain) {
        super(serviceUrl);

        login = userName;
        this.token = token;
        this.account = account;
        this.domain = domain;
    }

    public List<EmailBox> getEmailBoxes() throws IOException {
        Map<String, String> params = new HashMap<>();
        params.put(USER_NAME_PARAM, login);
        params.put(TOKEN_PARAM, token);
        params.put(ACCOUNT_PARAM, account);
        params.put(DOMAIN_PARAM, domain);
        params.put(CLASS_PARAM, EMAIL_CLASS);
        params.put(METHOD_PARAM, INFO_METHOD);


        EmailsResponse response = super.execute(params, EmailsResponse.class);

        return response.getData().stream()
                .map(EmailBox::new)
                .collect(Collectors.toList());
    }

}
