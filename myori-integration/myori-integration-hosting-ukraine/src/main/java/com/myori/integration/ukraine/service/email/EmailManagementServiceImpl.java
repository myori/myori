package com.myori.integration.ukraine.service.email;

import com.myori.integration.core.service.details.IntegrationServiceDetails;
import com.myori.integration.ukraine.model.email.Details.EmailBoxDetails;
import com.myori.integration.ukraine.model.email.EmailBox;

public class EmailManagementServiceImpl implements EmailManagementService {

    @Override
    public void clearEmailBox(EmailBox emailBox) {

    }

    @Override
    public EmailBox createEmailBox(EmailBoxDetails emailBoxDetails) {
        return null;
    }

    @Override
    public void editEmailBox(EmailBox emailBox) {

    }

    @Override
    public EmailBoxDetails deleteEmailBox(EmailBox emailBox) {
        return null;
    }

    @Override
    public EmailBoxDetails getEmailBoxDetails(EmailBox emailBox) {
        return null;
    }

    @Override
    public IntegrationServiceDetails getDetails() {
        return null;
    }
}
