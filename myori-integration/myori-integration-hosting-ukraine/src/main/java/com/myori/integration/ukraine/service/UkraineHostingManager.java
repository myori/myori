package com.myori.integration.ukraine.service;

import com.myori.integration.core.service.details.IntegrationServiceDetails;
import com.myori.integration.ukraine.model.email.EmailBox;
import com.myori.integration.ukraine.service.detail.UkraineHostingServiceDetails;
import com.myori.integration.ukraine.service.remote.UkraineHostingRemoteEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service("ukraineHostingService")
public class UkraineHostingManager implements UkraineHostingService {
    
    private final UkraineHostingServiceDetails ukraineHostingServiceDetails;

    private final UkraineHostingRemoteEndpoint remoteEndpoint;

    @Autowired
    public UkraineHostingManager(UkraineHostingRemoteEndpoint remoteEndpoint) {
        this.remoteEndpoint = remoteEndpoint;
        ukraineHostingServiceDetails = new UkraineHostingServiceDetails(this);
    }

    @Override
    public IntegrationServiceDetails getDetails() {
        return ukraineHostingServiceDetails;
    }

    @Override
    public List<EmailBox> getEmailBoxes() throws IOException {
        return remoteEndpoint.getEmailBoxes();
    }
}
