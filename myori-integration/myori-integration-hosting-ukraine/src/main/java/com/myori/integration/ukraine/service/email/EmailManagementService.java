package com.myori.integration.ukraine.service.email;

import com.myori.integration.core.service.IntegrationService;
import com.myori.integration.core.utils.annotation.ServiceOperation;
import com.myori.integration.ukraine.model.email.EmailBox;
import com.myori.integration.ukraine.model.email.Details.EmailBoxDetails;

public interface EmailManagementService extends IntegrationService {

    @ServiceOperation(name = "Очистить ящик")
    void clearEmailBox(EmailBox emailBox);

    @ServiceOperation(name = "Создать ящик")
    EmailBox createEmailBox(EmailBoxDetails emailBoxDetails);

    @ServiceOperation(name = "Редактировать ящик")
    void editEmailBox(EmailBox emailBox);

    @ServiceOperation(name = "Удалить ящик")
    EmailBoxDetails deleteEmailBox(EmailBox emailBox);

    @ServiceOperation(name = "Информация")
    EmailBoxDetails getEmailBoxDetails(EmailBox emailBox);

}
