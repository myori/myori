package com.myori.integration.config;

import com.myori.integration.ukraine.service.remote.UkraineHostingRemoteEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.integration.annotation.IntegrationComponentScan;

@Configuration
@IntegrationComponentScan(value = "com.myori.integration.ukraine.service")
@ComponentScan("com.myori.integration.ukraine")
@PropertySource(name = "hosting_ukraine.properties", value = "classpath:hosting_ukraine.properties", encoding = "UTF-8")
public class UkraineConfig {

    private final Logger LOGGER = LoggerFactory.getLogger(UkraineConfig.class);

    @Autowired
    private Environment environment;

    public UkraineConfig() {
        LOGGER.info("Create MyOri-Ukraine-Hosting context configuration");
    }

    @Bean
    public UkraineHostingRemoteEndpoint ukraineHostingRemoteEndpoint() {
        return new UkraineHostingRemoteEndpoint(serviceUrl(), userName(),token(), account(), domain());
    }

    private String serviceUrl() {
        return environment.getProperty("service.url");
    }

    private String userName() {
        return environment.getProperty("auth.username");
    }

    private String token() {
        return environment.getProperty("auth.token");
    }

    private String account() {
        return environment.getProperty("hosting.account");
    }

    private String domain() {
        return environment.getProperty("domain");
    }

}
