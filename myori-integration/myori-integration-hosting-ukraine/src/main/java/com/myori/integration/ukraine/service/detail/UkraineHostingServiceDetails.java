package com.myori.integration.ukraine.service.detail;

import com.myori.integration.core.service.details.IntegrationServiceDetails;
import com.myori.integration.ukraine.service.UkraineHostingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UkraineHostingServiceDetails extends IntegrationServiceDetails<UkraineHostingService> {

    @Autowired
    public UkraineHostingServiceDetails(UkraineHostingService integrationService) {
        super(integrationService);
    }

    @Override
    protected int getOperationCount() {
        return 0;
    }
}
