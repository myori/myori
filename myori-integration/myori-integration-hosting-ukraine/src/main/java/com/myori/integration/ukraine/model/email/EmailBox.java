package com.myori.integration.ukraine.model.email;

import com.myori.integration.ukraine.model.email.Details.EmailBoxDetails;
import com.myori.integration.ukraine.model.email.Details.spam.EmailSpamPolicy;

public class EmailBox {

    private final EmailBoxDetails emailBoxDetails;

    private EmailSpamPolicy emailSpamPolicy;

    public EmailBox(EmailBoxDetails emailBoxDetails) {
        this.emailBoxDetails = emailBoxDetails;
    }

    public EmailBoxDetails getEmailBoxDetails() {
        return emailBoxDetails;
    }

    public EmailSpamPolicy getEmailSpamPolicy() {
        return emailSpamPolicy;
    }

    public void setEmailSpamPolicy(EmailSpamPolicy emailSpamPolicy) {
        this.emailSpamPolicy = emailSpamPolicy;
    }
}
