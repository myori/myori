package com.myori.integration.ukraine.service;

import com.myori.integration.core.service.IntegrationService;
import com.myori.integration.ukraine.model.email.EmailBox;

import java.io.IOException;
import java.util.List;

public interface UkraineHostingService extends IntegrationService {

    List<EmailBox> getEmailBoxes() throws IOException;

}
