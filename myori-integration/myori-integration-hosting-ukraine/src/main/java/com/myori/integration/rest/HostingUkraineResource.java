package com.myori.integration.rest;

import com.myori.integration.ukraine.model.email.EmailBox;
import com.myori.integration.ukraine.service.UkraineHostingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path("/ukraine")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Component
public class HostingUkraineResource {
    
    private final UkraineHostingService ukraineHostingService;

    @Autowired
    public HostingUkraineResource(UkraineHostingService ukraineHostingService) {
        this.ukraineHostingService = ukraineHostingService;
    }

    @GET
    @Path("/emailBoxes")
    public Response getEmailBoxes() {
        try {
            List<EmailBox> emailBoxes = ukraineHostingService.getEmailBoxes();
            return Response.ok().entity(emailBoxes).build();
        } catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
