package com.myori.integration.core.service;

import com.myori.integration.core.service.details.IntegrationServiceDetails;

public interface IntegrationService {

    IntegrationServiceDetails getDetails();

}
