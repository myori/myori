package com.myori.integration.core.service.details;

import com.myori.integration.core.service.IntegrationService;
import com.myori.integration.core.service.details.operation.ServiceOperation;
import com.myori.integration.core.service.details.operation.ServiceOperationFilter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class IntegrationServiceDetails<T extends IntegrationService> {

    private final T serviceInstance;

    public IntegrationServiceDetails(T integrationService) {
        this.serviceInstance = integrationService;
    }

    protected List<ServiceOperation> getOperations() {
        Arrays.stream(serviceInstance.getClass().getMethods())
                .filter(ServiceOperationFilter::accept)
                .collect(Collectors.toList());

        return null;
    }

    protected abstract int getOperationCount();

}
