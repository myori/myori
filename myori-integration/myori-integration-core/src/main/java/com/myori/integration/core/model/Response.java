package com.myori.integration.core.model;

public interface Response<T> {

    T getData();

    void setData(T data);
}
