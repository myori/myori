package com.myori.integration.core.model;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

@Component
public class AuthChannel implements SubscribableChannel {


    @Override
    public boolean subscribe(MessageHandler handler) {
        return false;
    }

    @Override
    public boolean unsubscribe(MessageHandler handler) {
        return false;
    }

    @Override
    public boolean send(Message<?> message) {
        return false;
    }

    @Override
    public boolean send(Message<?> message, long timeout) {
        return false;
    }
}
