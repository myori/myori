package com.myori.integration.core.service.details.operation;

import com.myori.integration.core.utils.annotation.ServiceOperation;

import java.lang.reflect.Method;

public class ServiceOperationFilter {

    public static boolean accept(Method method) {
        return method != null && method.isAnnotationPresent(ServiceOperation.class);
    }

}
