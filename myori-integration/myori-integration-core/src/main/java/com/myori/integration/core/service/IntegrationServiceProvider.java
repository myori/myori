package com.myori.integration.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.Map;

public final class IntegrationServiceProvider {

    private final ApplicationContext applicationContext;

    @Autowired
    public IntegrationServiceProvider(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public Map<String, IntegrationService> listIntegrationServices() {
        return applicationContext.getBeansOfType(IntegrationService.class);
    }

}
