package com.myori.integration.core.service.transport.context.payload.remote;

import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class RestEndpoint extends RemoteEndpoint {

    private final RestTemplate client;

    protected RestEndpoint(String uri, RestTemplate client) {
        super(uri);
        this.client = client;
        this.client.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    }

    public RestEndpoint(String uri, ClientHttpRequestFactory clientHttpRequestFactory) {
        this(uri, new RestTemplate(clientHttpRequestFactory));
    }

    public RestEndpoint(String uri) {
        this(uri, new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build()));
    }

    @Override
    protected RestTemplate getClient() {
        return client;
    }


}
