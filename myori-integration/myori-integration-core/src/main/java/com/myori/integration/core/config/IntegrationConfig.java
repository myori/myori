package com.myori.integration.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;

@Configuration
@EnableIntegration
@IntegrationComponentScan("com.myori.integration.core")
@ComponentScan("com.myori.integration")
public class IntegrationConfig {

    private static final int MAX_AUTH_REQUESTS = 100;
    private static final String LOG_CHANNEL_NAME = "logChannel";
    private static final String TCP_INPUT_NAME = "toTcp.input";
    private static final String SECURITY_INTERCEPTOR_NAME = "channelSecurityInterceptor";
    
    private final Logger LOGGER = LoggerFactory.getLogger(IntegrationConfig.class);
/*
    private final AccessDecisionManager accessDecisionManager;
    
    private final AuthenticationManager authenticationManager;

    @Autowired
    public IntegrationConfig(AccessDecisionManager accessDecisionManager, AuthenticationManager authenticationManager) {
        this.accessDecisionManager = accessDecisionManager;
        this.authenticationManager = authenticationManager;
        LOGGER.info("Create MyOri-Integration-Core context configuration");
    }

    @Bean
    @SecuredChannel(interceptor = SECURITY_INTERCEPTOR_NAME, sendAccess = Roles.ADMINISTRATOR, receiveAccess = Roles.ADMINISTRATOR)
    public SubscribableChannel adminChannel() {
        PublishSubscribeChannel channel = new PublishSubscribeChannel();
        channel.setBeanName("adminChannel");

        return channel;
    }

    @Bean
    @SecuredChannel(interceptor = SECURITY_INTERCEPTOR_NAME, sendAccess = Roles.ANONYMOUS)
    public PollableChannel authenticationChannel() {
        LOGGER.info("Creating auth channel");
        return new QueueChannel(MAX_AUTH_REQUESTS);
    }

    @MessagingGateway(defaultRequestChannel = TCP_INPUT_NAME)
    public interface ToTCP {
        void send(String data, @Header("host") String host, @Header("port") int port);
    }

    @MessagingGateway(defaultRequestChannel = LOG_CHANNEL_NAME)
    public interface MyOriLoggingGateway {
        void sendToLogger(String data);
    }
    
    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(5);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("MyOri-");
        executor.initialize();
        
        return executor;
    }
    
    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler(new ScheduledThreadPoolExecutor(4));
    }
    
    @Bean
    public PublishSubscribeChannel errorChannel() {
        return new PublishSubscribeChannel();
    }
    
    @Bean
    public ChannelSecurityInterceptor channelSecurityInterceptor() {
        ChannelSecurityInterceptor interceptor = new ChannelSecurityInterceptor();
        interceptor.setAccessDecisionManager(accessDecisionManager);
        //interceptor.setAuthenticationManager(authenticationManager);
        
        return interceptor;
    }
*/
}
