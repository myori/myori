package com.myori.integration.core.service;

import com.myori.integration.core.service.details.IntegrationServiceDetails;
import com.myori.security.providers.remote.RemoteAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class RemoteAuthenticationService implements RemoteAuthenticationProvider, IntegrationService {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return false;
    }

    @Override
    public IntegrationServiceDetails getDetails() {
        return null;
    }
}
