package com.myori.integration.core.service.transport.context.payload.remote;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Map;

public abstract class RemoteEndpoint  {

    private final String baseUrl;

    protected RemoteEndpoint(String uri) {
        baseUrl = uri;
    }

    protected final <T> T execute(Map<String, String> params, Class<T> clazz, Class<?>... parameterClasses) throws IOException {
        MappingJacksonValue value = new MappingJacksonValue(params);
        value.setSerializationView(params.getClass());

        HttpEntity<MappingJacksonValue> entity = new HttpEntity<>(value);
        String res = getClient().postForObject(baseUrl, entity, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(clazz, parameterClasses);

        return objectMapper.readValue(res, type);
    }

    protected abstract RestTemplate getClient();

    protected String getBaseUri() {
        return baseUrl;
    }

}
