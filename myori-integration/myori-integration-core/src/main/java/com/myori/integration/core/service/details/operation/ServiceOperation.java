package com.myori.integration.core.service.details.operation;

public interface ServiceOperation {

    String getName();

    String getInternalName();
}
