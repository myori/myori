import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AppModule} from './app/app.module';
import "reflect-metadata";

platformBrowserDynamic().bootstrapModule(AppModule).then(() => {
    console.log('Loading MyOri Client ...');
}).catch((error: any) => console.log(error));
