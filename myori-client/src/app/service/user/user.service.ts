import {Injectable} from '@angular/core';
import {UpdateUserPassword, User, UserPassword, UserStatus} from '../../model/user/user';
import {UserDataTableModel, UserSearch} from '../../model/user/datatable/user.datatable';
import HttpUtils, {handleError} from '../../utils/http-utils';
import {BaseService} from '../base.service';
import {Credentials} from '../../utils/constants';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class UserService extends BaseService<UserSearch, UserDataTableModel> {
    private readonly baseUserUrl: string = Credentials.baseApiUrl + 'user';  // URL to user web API
    private readonly restorePasswordUrl: string = this.baseUserUrl + '/restore-password';
    private readonly resetPasswordUrl: string = this.baseUserUrl + '/reset-password';

    constructor (private http: HttpClient) {
        super();
    }

    public activateUser(user: User): Observable<User> {
        return this.http.put<User>(this.baseUserUrl + '/activate', user, HttpUtils.getOptions()).pipe(
            catchError(handleError)
        );
    }

    public findOne(id: number): Observable<User> {
        return this.http.get<User>(this.baseUserUrl + '/' + id, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public findByCriteria(page: number, pageSize: number, userSearch: UserSearch, sortField: string | string[] = '', sortOrder = 'UNSORTED', preload = true): Observable<UserDataTableModel> {
        return this.http.get<UserDataTableModel>(this.baseUserUrl, HttpUtils.getOptions(this.createSearchUsersParam(page, pageSize, sortField, sortOrder, userSearch)))
            .pipe(catchError(handleError));
    }

    public register(user: User): Observable<User> {
        return this.http.post<User>(this.baseUserUrl, user, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public save(user: User): Observable<User> {
        return this.http.put<User>(`${this.baseUserUrl}`, user, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public massRemove(ids: number[]): Observable<any> {
        return this.http.post(`${this.baseUserUrl}/mass-delete`, ids, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public massBlock(ids: number[]): Observable<any> {
        return this.http.post(`${this.baseUserUrl}/mass-block`, ids, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public massUnBlock(ids: number[]): Observable<any> {
        return this.http.post(`${this.baseUserUrl}/mass-unblock`, ids, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public resendNotification(ids: number[]): Observable<any> {
        return this.http.post(this.baseUserUrl + '/send-notification', ids, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public findByEmail(email: string): Observable<User> {
        let userSearch: UserSearch = new UserSearch;
        userSearch.emailFull = email;
        return this.findByCriteria(0, 1, userSearch, undefined, undefined, false).pipe(
            map(value => value.rows == 1 ? value.data[0] : null)
        );
    }

    public findByName(name: string): Observable<User> {
        let userSearch: UserSearch = new UserSearch;
        userSearch.name = name;
        return this.findByCriteria(0, 1, userSearch, undefined, undefined, false).pipe(
            map(value => value.rows == 1 ? value.data[0] : null)
        );
    }

    public updatePassword(request: UpdateUserPassword, userId: number): Observable<Object> {
        return this.http.put(this.baseUserUrl + `/${userId}/password`, request, HttpUtils.getOptions());
    }

    public requestRestorePasswordNotification(authData: string): Observable<Object> {
        return this.http.put(this.restorePasswordUrl, authData, HttpUtils.getOptions());
    }

    public resetPassword(newPassword: UserPassword): Observable<Object> {
        return this.http.put(this.resetPasswordUrl, newPassword, HttpUtils.getOptions());
    }

    private createSearchUsersParam(page: number, pageSize: number, sortField: string | string[], sortOrder: string, userSearch: UserSearch): HttpParams {
        return new HttpParams()
        .append('page', page.toString())
        .append('pageSize', pageSize.toString())
        .append('sortField', sortField.toString())
        .append('sortOrder', sortOrder)
        .append('fio', userSearch.fio)
        .append('email', userSearch.email)
        .append('emailFull', userSearch.emailFull)
        .append('roleId', userSearch.roleId.toString())
        .append('userStatus',  UserStatus[userSearch.userStatus])
        .append('name', userSearch.name)
        .append('phone', userSearch.phone)
        .append('creationDateFrom', userSearch.creationDateFrom.toDateString())
        .append('creationDateTo', userSearch.creationDateTo.toDateString());
    }

}
