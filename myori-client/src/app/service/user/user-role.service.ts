import {Injectable} from '@angular/core';
import {Credentials} from '../../utils/constants';
import HttpUtils, {handleError} from '../../utils/http-utils';
import {PermissionTreeNode, UserRole} from '../../model/user-role/user-role';
import {User} from '../../model/user/user';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';

@Injectable()
export class UserRoleService {
    private baseUserRoleUrl: string = Credentials.baseApiUrl + 'user-role';

    constructor(private http: HttpClient) {}

    public findOne(id: number): Observable<UserRole> {
        return this.http.get<UserRole>(this.baseUserRoleUrl + '/' + id, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public findAll(): Observable<UserRole[]> {
        return this.http.get<UserRole[]>(this.baseUserRoleUrl, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public findByName(name: string): Observable<User> {
        return this.http.get<User>(this.baseUserRoleUrl + '/get-by-name', HttpUtils.getOptions(new HttpParams().append('name', name)))
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    if (error.status === 404) {
                        return throwError(null);
                    }
                    return handleError(error);
                })
            );
    }

    public save(userRole: UserRole): Observable<UserRole> {
        return this.http.post<UserRole>(this.baseUserRoleUrl, userRole, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public delete(id: number): Observable<any> {
        return this.http.delete(this.baseUserRoleUrl + '/' + id, HttpUtils.getOptions()).pipe(catchError(handleError));
    }

    public getPermissionTree(): Observable<PermissionTreeNode[]> {
        return this.http.get<PermissionTreeNode[]>(this.baseUserRoleUrl + '/permissions-tree', HttpUtils.getOptions())
            .pipe(catchError(handleError));
    }

}
