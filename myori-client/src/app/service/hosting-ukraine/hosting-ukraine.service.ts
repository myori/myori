import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EmailBox} from '../../model/hosting-ukraine/hosting-ukraine';
import HttpUtils from '../../utils/http-utils';

@Injectable()
export class HostingUkraineService {

    private readonly baseUrl: string = 'http://localhost:8080/api/rest/ukraine';

    constructor(private http: HttpClient) {}

    getEmailBoxes(): Observable<EmailBox[]> {
        return this.http.get<EmailBox[]>(this.baseUrl + '/emailBoxes', HttpUtils.getOptions());
    }
}
