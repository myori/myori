import {HttpResponse} from '@angular/common/http';

export declare type AuthErrorResponse<T> = NotAuthenticatedErrorResponse;

export class NotAuthenticatedErrorResponse extends HttpResponse<JSON> implements Error {

    message: string;
    name: string;
    stack: string;

    constructor(url: string) {
        super({url: url, status: 401, statusText: 'User is not authenticated'});
    }

}

export class NotAuthorizedErrorResponse extends HttpResponse<JSON> implements Error {

    message: string;
    name: string;
    stack: string;

    constructor(url: string) {
        super({url: url, status: 403, statusText: 'Not authorized operation'});
    }

}
