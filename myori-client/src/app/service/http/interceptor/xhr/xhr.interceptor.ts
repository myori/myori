import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MessageService} from '../../../message/message.service';

@Injectable()
export class XhrInterceptor implements HttpInterceptor {

    constructor(private messenger: MessageService) {
        this.messenger.add('CREATE XhrInterceptor');
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const xhr = req.clone({ headers: req.headers.set('X-Requested-With', 'XMLHttpRequest') });
        return next.handle(xhr);
    }

}
