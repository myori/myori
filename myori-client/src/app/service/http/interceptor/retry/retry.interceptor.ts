import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {filter, retry} from 'rxjs/operators';
import {MessageService} from '../../../message/message.service';

@Injectable()
export class RetryInterceptor implements HttpInterceptor {

    constructor(private messenger: MessageService) {
        this.messenger.addTrace("CREATE RetryInterceptors");
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            retry(3)
        );
    }

}
