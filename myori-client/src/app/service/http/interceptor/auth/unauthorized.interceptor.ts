import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {AuthService} from '../../../auth/auth.service';
import {tap} from 'rxjs/operators';
import {MessageService} from '../../../message/message.service';
import {TOKEN_PROP_NAME} from '../../common/constants';
import {serialize} from 'class-transformer';
import {NotAuthenticatedErrorResponse} from '../../event/auth.event';

@Injectable()
export class UnauthorizedInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService, private messenger: MessageService) {
        this.messenger.addTrace("CREATE UnauthorizedInterceptor");
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return req.headers.has(TOKEN_PROP_NAME)
            ? next.handle(req).pipe(tap(this.handler, this.authError))
            : of(new NotAuthenticatedErrorResponse(req.url));
    }

    private readonly handler = (event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
            this.messenger.addTrace(serialize({response: event}));
        }
    };

    private readonly authError = (err: any) => {
        if (err instanceof HttpErrorResponse) {
            switch (err.status) {
                case 401:
                case 403:
                    this.authService.authenticate();
            }
        }
    }

}
