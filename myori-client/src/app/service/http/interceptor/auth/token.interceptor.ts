import {Injectable} from '@angular/core';
import {HttpEvent, HttpEventType, HttpHandler, HttpHeaderResponse, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../../../auth/auth.service';
import {MessageService} from '../../../message/message.service';
import {tap} from 'rxjs/operators';
import {TOKEN_PROP_NAME, USER_ID_PROP_NAME} from '../../common/constants';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService, private messenger: MessageService) {
        this.messenger.addTrace("CREATE TokenInterceptors");
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authToken = this.authService.getAuthorizationToken();
        const authReq = authToken ? req.clone({ headers: req.headers.set(TOKEN_PROP_NAME, authToken.token) }) : req;

        return next.handle(authReq).pipe(
            tap(this.handel, this.handleError)
        );
    }

    private handel(e: HttpEvent<any>) {
        e && e.type === HttpEventType.ResponseHeader ? this.updateAuthToken(e) : null;
    }

    private handleError(e: HttpEvent<any>) {
        e && e.type === HttpEventType.ResponseHeader ? this.updateAuthToken(e) : null;
    }

    private updateAuthToken(h: HttpHeaderResponse) {
        h && h ? this.authService.setAuthorizationToken({userId: h.headers.get(USER_ID_PROP_NAME), token: h.headers.get(TOKEN_PROP_NAME)}) : null;
    }

}
