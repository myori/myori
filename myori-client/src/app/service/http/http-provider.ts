import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {XhrInterceptor} from './interceptor/xhr/xhr.interceptor';
import {TokenInterceptor} from './interceptor/auth/token.interceptor';
import {LoggingInterceptor} from './interceptor/logging/logging.interceptor';
import {RetryInterceptor} from './interceptor/retry/retry.interceptor';
import {UnauthorizedInterceptor} from './interceptor/auth/unauthorized.interceptor';

export const HTTP_INTERCEPTOR_PROVIDERS = [
    { provide: HTTP_INTERCEPTORS, useClass: LoggingInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RetryInterceptor, multi: true},

    //inbound
    { provide: HTTP_INTERCEPTORS, useClass: UnauthorizedInterceptor, multi: true}
];
