import {Observable} from 'rxjs';

export abstract class BaseService<T, R> {

    protected abstract findByCriteria(page: number, pageSize: number, searchModel: T, sortField: string, sortOrder: string): Observable<R>;
}
