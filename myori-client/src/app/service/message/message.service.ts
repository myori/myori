import {Injectable} from '@angular/core';

@Injectable()
export class MessageService {

    add(msg: string) {
        console.log(msg);
    }

    addError(msg: string) {
        console.error(msg);
    }

    addTrace(msg: string) {
        true ? this.add(msg) : null;
    }

}
