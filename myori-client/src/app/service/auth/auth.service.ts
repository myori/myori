import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {RouteLinks} from '../../utils/routes';
import {finalize} from 'rxjs/operators';
import {Authentication} from '../../model/auth/authentication';

interface AuthToken {
    readonly userId: string;
    readonly token: string;
}

@Injectable()
export class AuthService {

    private readonly uri: string = '/api/rest';
    private authToken: AuthToken;
    authenticated = false;

    constructor(private http: HttpClient, private router: Router) {}

    getAuthorizationToken(): AuthToken {
        return this.authToken;
    }

    setAuthorizationToken(authToken: AuthToken) {
        this.authToken = authToken;
    }

    authenticate() {
        this.router.navigate([RouteLinks.LOGIN_PAGE_LINK]);
    }

    sendRequest(): Observable<Object> {
        return this.http.get(this.uri);
    }

    login(auth: Authentication) {
        this.http.post(this.uri + '/auth/login', auth).subscribe(response => {
            if (response['name']) {
                this.authenticated = true;
            } else {
                this.authenticated = false;
            }
        }, () => { this.authenticated = false; });

    }

    logout()  {
        this.http.post(this.uri + '/auth/logout', {})
            .pipe(
                finalize(() => this.authenticated = false)
            ).subscribe();
    }

}
