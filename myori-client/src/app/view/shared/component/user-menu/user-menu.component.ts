import {Component} from '@angular/core';

@Component({
    selector: 'myori-user-menu',
    templateUrl: 'user-menu.component.html',
    styleUrls: ['user-menu.component.css']
})
export class UserMenuComponent {

    active = false;

    toggleMenu() {
        this.active = !this.active;
    }
}
