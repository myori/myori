import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'crm-error-messages',
    templateUrl: 'error-messages.component.html',
    styleUrls: ['error-messages.component.css']
})
export class ErrorMessagesComponent {

    @Input()
    control: FormControl | string;
    constructor() { }

    get errorMessage() {
        if (this.control instanceof FormControl) {
            for (let errorKey in this.control.errors) {
                if (this.control.errors.hasOwnProperty(errorKey) && this.control.touched) {
                    if (errorKey === 'minlength')  {
                        return 'ui.messages.error.' + errorKey + '.' + this.control.errors.minlength.requiredLength;
                    }
                    if (errorKey === 'maxlength')  {
                        return 'ui.messages.error.' + errorKey + '.' + this.control.errors.maxlength.requiredLength;
                    }
                    return 'ui.messages.error.' + errorKey;
                }
            }
            return null;
        } else {
            return this.control;
        }
    }
}
