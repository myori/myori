import {NgModule} from '@angular/core';
import {ErrorComponent} from './component/error.component';
import {CommonModule} from '@angular/common';

@NgModule({
    declarations: [
        ErrorComponent
    ],
    exports: [
        ErrorComponent
    ],
    imports: [
        CommonModule
    ]
})
export class ErrorModule {
}
