import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {HtmlUtils} from '../../../../utils/html-utils';

@Component({
    selector: 'crm-dialog',
    templateUrl: 'base-dialog.component.html',
    styleUrls: ['base-dialog.component.css']
})
export class BaseDialogComponent {


    @Input() dialogTitleKey: string;
    @Input() showDialog: boolean;
    @Input() dialogForm: FormGroup;
    @Input() type: DialogType;

    @Output() onHideDialog = new EventEmitter();
    @Output() onSubmitDialog = new EventEmitter();

    readonly DialogType = DialogType;

    onShow() {
        HtmlUtils.appendClassToBody('body-fixed');
    }

    onHide() {
        HtmlUtils.removeClassFromBody('body-fixed');
        this.onHideDialog.emit();
    }

    submit() {
        this.onSubmitDialog.emit();
    }
}

export enum DialogType {
    SEARCH,
    EDIT
}
