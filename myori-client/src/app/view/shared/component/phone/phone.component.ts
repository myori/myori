import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormControl} from '@angular/forms';
import {PHONE_MASK, PHONE_REGEXP} from '../../../../utils/constants';

@Component({
    selector: 'crm-phone',
    templateUrl: 'phone.component.html',
    styleUrls: ['phone.component.css']
})
export class PhoneComponent implements OnInit {

    readonly PHONE_MASK = PHONE_MASK;

    @Input() phonesArray: FormArray;
    phoneControl: FormControl;

    ngOnInit() {
        if (this.phonesArray === undefined) {
            throw new Error('Form control \'phoneControl\' is required');
        }
        this.phonesArray.push(this.createControl(''));
    }

    addPhone() {
        if (this.isPhoneValid()) {
            this.phonesArray.push(this.createControl(''));
        } else {
            this.phoneControl.markAsTouched({ onlySelf: true });
        }
    }

    deleteItem(phone: FormControl) {
        this.phonesArray.removeAt(this.phonesArray.controls.indexOf(phone, 0));
    }

    get controls(): AbstractControl[] {
        return this.phonesArray ? this.phonesArray.controls.filter(control => control !== this.phoneControl) : [];
    }

    @Input()
    set phones(phones: string[]) {
        if (phones) {
            this.phonesArray.controls.splice(0, this.phonesArray.controls.length);
            phones.filter(phone => phone !== '').map(phone => this.createControl(phone)).forEach(phone => this.phonesArray.push(phone));
        }
    }

    validateIfNotEmpty() {
        if (!this.isEmptyControl() && !this.isPatternMatch()) {
            this.phoneControl.setErrors({ 'phone.pattern' : true });
        } else if (this.isPhoneExists()) {
            this.phoneControl.setErrors({ 'phone.duplicate': true });
        }
    }

    private createControl(value: string): FormControl {
        this.phoneControl = new FormControl(value);
        return this.phoneControl;
    }

    private isPhoneValid(): boolean {
        if (this.isEmptyControl()) {
            this.phoneControl.setErrors({ 'phone.empty': true });
        } else if (!this.isPatternMatch()) {
            this.phoneControl.setErrors({ 'phone.pattern' : true });
        } else if (this.isPhoneExists()) {
            this.phoneControl.setErrors({ 'phone.duplicate': true });
        }

        return this.phoneControl.valid;
    }

    private isPhoneExists(): boolean {
        return this.phonesArray.controls
            .filter(phone => phone !== this.phoneControl)
            .find(item => item.value === this.phoneControl.value) !== undefined;
    }

    private isEmptyControl(): boolean {
        return this.phoneControl.value == null || this.phoneControl.value.length === 0;
    }

    private isPatternMatch(): boolean {
        return PHONE_REGEXP.test(this.phoneControl.value);
    }


}
