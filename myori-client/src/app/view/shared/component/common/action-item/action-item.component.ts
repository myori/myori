import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'crm-action-item',
    templateUrl: 'action-item.component.html',
    styleUrls: ['action-item.component.css']
})
export class ActionItemComponent implements OnInit {

    @Input() actionItemControl: FormControl;

    @Output()
    onClick = new EventEmitter<FormControl>();

    actionPerformed() {
        this.onClick.emit(this.actionItemControl);
    }

    ngOnInit(): void {
        if (this.actionItemControl === null) {
            throw new Error('Property \'actionItemControl\' is required');
        }
    }

}
