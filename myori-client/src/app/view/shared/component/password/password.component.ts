import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CrmValidators} from '../../../../utils/crm-validators';

@Component({
    selector: 'crm-password',
    templateUrl: 'password.component.html',
    styleUrls: ['password.component.css']
})
export class PasswordComponent implements OnInit {

    readonly first_control_name: string = 'newPassword';
    readonly second_control_name: string = 'confirmationPassword';

    @Input() parentGroup: FormGroup;
    @Input() passwordLabel: string;
    @Input() confirmationPasswordLabel: string;

    constructor(private fb: FormBuilder) {}

    ngOnInit(): void {
        this.parentGroup.addControl(this.first_control_name, this.fb.control('', [Validators.required, Validators.minLength(6)]));
        this.parentGroup.addControl(this.second_control_name, this.fb.control('', [Validators.required, Validators.minLength(6),
                CrmValidators.valueEquals(this.passwordCtrl)]));
    }

    onPasswordBlur() {
        if (!this.passwordCtrl.pristine) {
             this.confirmCtrl.setErrors(CrmValidators.valueEquals(this.passwordCtrl)(this.confirmCtrl));
        }
    }

    get passwordCtrl() {
        return this.parentGroup.get(this.first_control_name);
    }

    get confirmCtrl() {
        return this.parentGroup.get(this.second_control_name);
    }

}
