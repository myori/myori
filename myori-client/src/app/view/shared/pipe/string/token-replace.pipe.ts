import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'replace_token',
    pure: true
})
export class TokenReplacePipe implements PipeTransform {

    private static mapTokens(token = '', map: Tokens[] = []): string {
        return TokenReplacePipe.isToken(token) ? TokenReplacePipe.replaceToken(TokenReplacePipe.unwrapToken(token), map) : token;
    }

    private static isToken(token: string): boolean {
        return token.startsWith('{{') && token.endsWith('}}');
    }

    private static unwrapToken(token = ''): string {
        return token.replace('{{', '').replace('}}', '');
    }

    private static replaceToken(token = '', params: Tokens[] = []): string {
        return params.find(tokenValue => tokenValue.key === token).value;
    }

    transform(value = '', params: Tokens[] = []): string {
        return value.split(/({{[a-zA-Z ]*}})/gi).map(token => TokenReplacePipe.mapTokens(token, params)).join('');
    }

}

export interface Tokens {
    key: string;
    value: any;
}
