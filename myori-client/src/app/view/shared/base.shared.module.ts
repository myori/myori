import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ErrorMessagesComponent} from './component/error-messages/error-messages.component';
import {SafePipe} from './pipe/dom/safepipe';
import {PasswordComponent} from './component/password/password.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ActionItemComponent} from './component/common/action-item/action-item.component';
import {ButtonModule} from 'primeng/button';
import {TokenReplacePipe} from './pipe/string/token-replace.pipe';
import {PhoneComponent} from './component/phone/phone.component';
import {BaseDialogComponent} from './component/dialog/base-dialog.component';
import {DialogModule} from 'primeng/dialog';
import {MenuModule} from 'primeng/menu';
import {ListboxModule} from 'primeng/listbox';
import {UserMenuComponent} from './component/user-menu/user-menu.component';
import {HttpClientModule} from '@angular/common/http';
import {LibUiComponentModule} from '../lib/lib.ui.component.module';
import {DropdownComponent} from '../lib/component/dropdown/dropdown.component';

@NgModule({
    imports: [CommonModule, ReactiveFormsModule, FormsModule,  HttpClientModule, LibUiComponentModule, ButtonModule, DialogModule, MenuModule,
        ListboxModule],
    declarations : [SafePipe, TokenReplacePipe, ErrorMessagesComponent, PasswordComponent, ActionItemComponent, PhoneComponent,
        BaseDialogComponent, UserMenuComponent],
    exports : [SafePipe, TokenReplacePipe, ErrorMessagesComponent, PasswordComponent, PhoneComponent, BaseDialogComponent,
        UserMenuComponent, DropdownComponent]
})
export class BaseSharedModule {

}
