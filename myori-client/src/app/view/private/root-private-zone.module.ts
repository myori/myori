import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RootPrivateZoneRoutingModule} from './root-private-zone.routing.module';
import {RootPrivateZoneComponent} from './component/root-private-zone.component';
import {BaseSharedModule} from '../shared/base.shared.module';
import {HostingUkraineModule} from './module/hosting-ukraine/hosting-ukraine.module';

@NgModule({
    imports: [CommonModule, RootPrivateZoneRoutingModule, BaseSharedModule, HostingUkraineModule],
    declarations: [RootPrivateZoneComponent],
    exports: [RootPrivateZoneComponent]
})
export class RootPrivateZoneModule {}
