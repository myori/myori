import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RootPrivateZoneComponent} from './component/root-private-zone.component';

// Route Configuration
export const routes: Routes = [
    {
        path: '',
        component: RootPrivateZoneComponent,
        children: [
            {
                path: 'ukraine',
                loadChildren: './module/hosting-ukraine/hosting-ukraine.module#HostingUkraineModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RootPrivateZoneRoutingModule {}
