import {NgModule} from '@angular/core';
import {HostingUkraineRoutingModule} from './hosting-ukraine.routing.module';
import {HostingUkraineComponent} from './component/hosting-ukraine.component';
import {HostingUkraineService} from '../../../../service/hosting-ukraine/hosting-ukraine.service';
import {CommonModule} from '@angular/common';
import {EmailsTableComponent} from './component/email/emails-table.component';
import {TableModule} from 'primeng/table';

@NgModule({
    imports: [CommonModule, HostingUkraineRoutingModule, TableModule],
    declarations: [HostingUkraineComponent, EmailsTableComponent],
    providers: [HostingUkraineService]
})
export class HostingUkraineModule {
}
