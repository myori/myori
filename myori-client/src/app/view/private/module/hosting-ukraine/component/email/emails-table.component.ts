import {Component, OnInit} from '@angular/core';
import {HostingUkraineService} from '../../../../../../service/hosting-ukraine/hosting-ukraine.service';
import {emailBoxArrToDetailsArr, EmailBoxDetails} from '../../../../../../model/hosting-ukraine/hosting-ukraine';
import {map} from 'rxjs/operators';

@Component({
    selector: 'hosting-email-boxes',
    templateUrl: 'emails-table.component.html'
})
export class EmailsTableComponent implements OnInit {
    details: EmailBoxDetails[];
    cols: any[];

    private readonly detailsAccessor = (details: EmailBoxDetails[] = []) => this.details = details;

    constructor(private hostingUkraineService: HostingUkraineService) {}

    ngOnInit(): void {
        this.hostingUkraineService.getEmailBoxes()
            .pipe(map(emailBoxArrToDetailsArr))
            .subscribe(this.detailsAccessor);

        this.cols = [
            { field: 'id', header: 'ID' },
            { field: 'name', header: 'Name' },
            { field: 'password', header: 'Password' },
            { field: 'type', header: 'Type' },
            { field: 'autospam', header: 'Auto spam' }
        ];
    }

}
