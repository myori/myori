import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HostingUkraineComponent} from './component/hosting-ukraine.component';

// Route Configuration
export const routes: Routes = [
    {path: '', component: HostingUkraineComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HostingUkraineRoutingModule {
}
