import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../service/auth/auth.service';
import {MessageService} from '../../../service/message/message.service';

@Component({
    selector: 'private-zone',
    templateUrl: 'root-private-zone.component.html'
})
export class RootPrivateZoneComponent implements OnInit {

    constructor(private authService: AuthService, private messenger: MessageService) {}

    ngOnInit(): void {
        this.authService.sendRequest().subscribe(
            msg => this.messenger.add(msg as string)
        );
    }
}
