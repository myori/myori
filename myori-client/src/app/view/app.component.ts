import {Component} from "@angular/core";

@Component({
    selector: 'myori-client-app',
    templateUrl: 'app.component.html',
    styles: ['myori-client-app { height: 100% }']
})
export class AppComponent {
    
}