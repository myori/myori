import {Component, Input} from '@angular/core';

@Component({
    selector: 'myori-dropdown',
    templateUrl: 'dropdown.component.html',
    styleUrls: ['dropdown.component.css']
})
export class DropdownComponent {

    @Input() options: Array<any> = [];
    @Input() title: string = '';

    private active = false;

    toggle() {
        this.active = !this.active;
    }

}
