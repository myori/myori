import {NgModule} from '@angular/core';
import {DropdownComponent} from './component/dropdown/dropdown.component';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [CommonModule],
    declarations: [DropdownComponent],
    exports: [DropdownComponent]
})
export class LibUiComponentModule {}
