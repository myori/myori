import {Component, OnInit} from '@angular/core';
import {Authentication} from '../../../../model/auth/authentication';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AuthService} from '../../../../service/auth/auth.service';
import {plainToClass} from 'class-transformer';

@Component({
    selector: 'myori-login-form',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

    authForm: FormGroup;
    credentials = new Authentication();

    constructor(private fb: FormBuilder, private authService: AuthService) {}

    login(): void {
        this.authService.login(plainToClass(Authentication, this.authForm.value as Object));
    }

    ngOnInit(): void {
        this.authForm = this.fb.group({
            username: '',
            password: ''
        });
    }
}
