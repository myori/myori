import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RootPublicZoneRoutingModule} from './root-public-zone.routing.module';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './component/login/login.component';
import {RootPublicZoneComponent} from './component/root-public-zone.component';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, RootPublicZoneRoutingModule],
    declarations: [RootPublicZoneComponent, LoginComponent],
    exports: [RootPublicZoneComponent],
})
export class RootPublicZoneModule {}
