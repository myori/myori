import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RoutePaths} from '../../utils/routes';
import {LoginComponent} from './component/login/login.component';

// Route Configuration
export const routes: Routes = [
    {path: RoutePaths.ROOT, redirectTo: RoutePaths.LOGIN, pathMatch: 'full'},
    {path: RoutePaths.LOGIN, component: LoginComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RootPublicZoneRoutingModule {
}
