export const Credentials = {
    baseApiUrl: '/api/external/v1/',
};

export const PHONE_MASK = ['+', '3', '8', ' ', '(', '0', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
export const ACTIVATION_KEY_MASK = [/[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/, '-', /[A-Za-z0-9]/, /[A-Za-z0-9]/,
    /[A-Za-z0-9]/, /[A-Za-z0-9]/, '-', /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/, '-', /[A-Za-z0-9]/, /[A-Za-z0-9]/,
    /[A-Za-z0-9]/, /[A-Za-z0-9]/, '-', /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/];
export const EAV_LOGIN_MASK = [/[A-Za-z]/, /[A-Za-z]/, /[A-Za-z]/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/,
    /\d/];
export const AUTH_CODE_MASK = [/[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];

export const PHONE_REGEXP: RegExp = /^\+38 \([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}$/i;
export const LICENESE_KEY_REGEXP: RegExp = /^([A-Za-z0-9]{4}-){4}[A-Za-z0-9]{4}$/i;
export const LOGIN_REGEXP: RegExp = /^[A-Za-z]{3}-[0-9]{8,12}$/i;
export const AUTH_CODE_REGEXP: RegExp = /^[0-9]{2}-[0-9]{2}$/i;

export const CONFIG_TABLE_MIN_ROWS = 10;
export const CONFIG_TABLE_ROWS_PER_PAGE_OPTIONS: number[] = [10, 25, 50, 100];
