export class HtmlUtils {
    public static appendClassToBody(className: string) {
        if (document && document.body) {
            let orig = document.body.className;
            document.body.className = orig + (orig ? ' ' : '') + className;
        }
    }
    public static removeClassFromBody(className: string) {
        if (document && document.body) {
            document.body.className = document.body.className.replace(new RegExp(' ?' + className, 'g'), '');
        }
    }
}
