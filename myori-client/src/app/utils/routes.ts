export class RoutePaths {
    public static readonly ROOT = '';
    public static readonly AUTH = 'auth';
    public static readonly LOGIN = 'login';
    public static readonly ADMINISTRATION = 'administration';
    public static readonly USERS = 'users';
    public static readonly USER_ROLES = 'roles';
    public static readonly CONFIRM_REGISTRATION = 'registration';
    public static readonly RESTORE_PASSWORD = 'restore-password';
    public static readonly FORBIDDEN = 'forbidden';
}

export class RouteLinks {
    public static readonly LOGIN_PAGE_LINK = '/' + RoutePaths.AUTH + '/' + RoutePaths.LOGIN;
    public static readonly DASHBOARD_PAGE_LINK = '/' + RoutePaths.ADMINISTRATION + '/' + RoutePaths.USERS;
    public static readonly FORBIDDEN_PAGE_LINK = '/';
}
