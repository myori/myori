import {HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {throwError} from 'rxjs';

export default class HttpUtils {

    static getOptions(param: HttpParams = new HttpParams()) {
        return { headers: HttpUtils.getHeaders(), param: param };
    }

    static getHeaders() {
        return defHeaders();
    }
}

function defHeaders(): HttpHeaders {
    return new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append('Accept', 'application/json');
}

export function handleError(response: HttpErrorResponse) {
    let errorMsg = response.message || `Problem was obtained when use service!`;
    console.error(errorMsg);
    return throwError(errorMsg);
}
