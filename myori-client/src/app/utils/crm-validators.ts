import {AbstractControl, ValidatorFn} from '@angular/forms';

export class CrmValidators {

    public static valueEquals(target: AbstractControl): ValidatorFn {
        return (control: AbstractControl) => {
            return control.value === target.value
                ? null
                : { 'notEquals': true };
        }
    }

}
