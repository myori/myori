import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RoutePaths} from './utils/routes';

export const routes: Routes = [
    {
        path: RoutePaths.ROOT,
        loadChildren: './view/private/root-private-zone.module#RootPrivateZoneModule'
    },
    {
        path: RoutePaths.AUTH,
        loadChildren: './view/public/root-public-zone.module#RootPublicZoneModule'
    },
    {
        path: RoutePaths.ROOT,
        redirectTo: RoutePaths.AUTH,
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
