import {Exclude, Transform, Type} from 'class-transformer';
import {UserRole} from '../user-role/user-role';

export class User {

    id: number;
    name: string;
    email: string;
    firstName: string;
    lastName: string;
    middleName: string;
    @Transform(value => UserStatus[value], { toClassOnly: true })
    status: UserStatus;
    creationDate: Date;
    @Type(() => UserRole)
    role: UserRole;
    phones: string[];
    password: string;
    isActionAvailable(action: UserAction) {
        switch (action) {
            case UserAction.REACTIVATE:
                return this.status === UserStatus.SUSPENDED;
            case UserAction.BLOCK:
                return this.status === UserStatus.ACTIVE;
            case UserAction.ACTIVATE:
            case UserAction.RESEND:
                return this.status === UserStatus.PENDING;
            default:
                return true;
        }
    }
    public get fio(): string {
        return `${this.lastName ? this.lastName : ''} ${this.firstName ? this.firstName : ''} ${this.middleName ? this.middleName : ''}`
            .trim();
    }
}

export enum UserStatus {
    PENDING,
    ACTIVE,
    SUSPENDED
}
export const USER_STATUSES = [UserStatus.PENDING, UserStatus.ACTIVE, UserStatus.SUSPENDED];

export function isMultiAction(action: UserAction) {
    return !(action === UserAction.EDIT || action === UserAction.ACTIVATE || action === UserAction.LOGIN_AS)
}

export enum UserAction {
    EDIT,
    DELETE,
    BLOCK,
    REACTIVATE,
    ACTIVATE,
    RESEND,
    LOGIN_AS
}

export const USER_ACTIONS: UserAction[] =
    [UserAction.EDIT, UserAction.DELETE, UserAction.BLOCK, UserAction.REACTIVATE,
        UserAction.ACTIVATE, UserAction.RESEND, UserAction.LOGIN_AS];

export class UserPassword {
    newPassword: string;

    @Exclude()
    confirmationPassword: string;

    isValid(): boolean {
        return this.newPassword && this.newPassword === this.confirmationPassword;
    }
}

export class UpdateUserPassword extends UserPassword {
    currentPassword: string;

    @Exclude()
    username: string;
}
