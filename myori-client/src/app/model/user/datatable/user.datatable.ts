import {User, UserStatus} from '../user';
import {Type} from 'class-transformer';
export class UserDataTableModel {
    @Type(() => User)
    data: User[];
    rows: number;
    errors: Error[];
}

export class UserSearch {

    keyword: string;
    fio: string;
    email: string;
    emailFull: string;
    roleId: number;
    userStatus: UserStatus;
    name: string;
    phone: string;
    creationDateFrom: Date;
    creationDateTo: Date;

    public setRoleId(id: any) {
        if (id === 0 || id === '0') {
            this.roleId = null;
        } else {
            this.roleId = id;
        }
    }

}
