import {Transform, Type} from 'class-transformer';

export class UserRole {

    id: number;
    name: string;
    @Transform(value => Role[value], { toClassOnly: true })
    roleType: Role;
    permissionKeys: string[] = [];
}

export enum Role {
    ROLE_SYSTEM_ADMIN,
    DYNAMIC_SYSTEM_ROLE
}

export class PermissionTreeNode {
    name: string;
    @Type(() => PermissionTreeNode)
    treeNodes: PermissionTreeNode[];
}
