export class EmailBox {
    emailBoxDetails: EmailBoxDetails;
}

export class EmailBoxDetails {
    id: number;
    name: string;
    password: string;
    type: string;
    autospam: string;
}

export function emailBoxToDetails(emailBox: EmailBox): EmailBoxDetails {
    return emailBox ? emailBox.emailBoxDetails : new EmailBoxDetails();
}

export function emailBoxArrToDetailsArr(arr: EmailBox[] = []): EmailBoxDetails[] {
    return arr.map(emailBoxToDetails);
}