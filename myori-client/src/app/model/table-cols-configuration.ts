export class TableColConfiguration {
    field: string;
    show: boolean;
    minColumnWidth: string;
    columnWidth: string;
    permanent: boolean;

    constructor(field: string, show: boolean, minColumnWidth: string, columnWidth: string, permanent = false) {
        this.field = field;
        this.show = show;
        this.minColumnWidth = minColumnWidth;
        this.columnWidth = columnWidth;
        this.permanent = permanent;
    }
}
