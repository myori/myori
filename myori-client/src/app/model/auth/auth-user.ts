export class AuthUser {

    id: number;
    name: string;

    permissions: string[] = [];

    public hasPermissions(permissions: string[]): boolean {
        return this.permissions.filter((auth) => permissions.indexOf(auth) >= 0).length > 0;
    }

    public hasPermission(permission: string): boolean {
        return this.permissions.indexOf(permission) >= 0;
    }
}

export interface AuthToken {
    token: string;
}
