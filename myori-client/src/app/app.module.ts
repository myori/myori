import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app.routing.module';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './view/app.component';
import {AuthService} from './service/auth/auth.service';
import {HTTP_INTERCEPTOR_PROVIDERS} from './service/http/http-provider';
import {HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {MessageService} from './service/message/message.service';
import {CommonModule} from '@angular/common';

const xsrfConfig = {
    cookieName: "XSRF-TOKEN",
    headerName: "XSRF-TOKEN"
};

@NgModule({
    imports: [CommonModule, BrowserModule, HttpClientModule, HttpClientXsrfModule.withOptions(xsrfConfig), AppRoutingModule],
    declarations: [AppComponent],
    providers: [AuthService, MessageService, HTTP_INTERCEPTOR_PROVIDERS],
    bootstrap: [AppComponent]
})
export class AppModule {}
