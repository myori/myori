package com.myori.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.myori")
public class MyOriCoreApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(MyOriCoreApplication.class, args);
    }
    
}
