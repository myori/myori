package com.myori.core.utils;

import com.myori.core.service.core.threading.Task;
import com.myori.core.service.core.threading.ThreadPool;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ThreadPoolUtils {
	/**
	 * Wraps an {@link ThreadPool} into an {@link Executor}
	 * 
	 * @param pool
	 *            the pool the to be wrapped
	 * @return the wrapped {@link Executor}
	 */
	public static ExecutorService wrap(final ThreadPool pool) {
		return new ExecutorService() {
			@Override
			public void execute(final Runnable command) {
				pool.async(wrap(command));
			}

			@Override
			public void shutdown() {
				pool.dispose();
			}

			@Override
			public List<Runnable> shutdownNow() {
				pool.dispose();
				return null;
			}

			@Override
			public boolean isShutdown() {
				return pool.isDisposed();
			}

			@Override
			public boolean isTerminated() {
				return pool.isDisposed();
			}

			@Override
			public boolean awaitTermination(long timeout, TimeUnit unit)
					throws InterruptedException {
				throw new UnsupportedOperationException();
			}

			@Override
			public <T> Future<T> submit(Callable<T> task) {
				return pool.async(wrap(task));
			}

			@Override
			public <T> Future<T> submit(Runnable task, T result) {
				return pool.async(wrap(Executors.callable(task, result)));
			}

			@Override
			public Future<?> submit(Runnable task) {
				return pool.async(wrap(task));
			}

			@Override
			public <T> List<Future<T>> invokeAll(
					Collection<? extends Callable<T>> tasks)
					throws InterruptedException {
				throw new UnsupportedOperationException();
			}

			@Override
			public <T> List<Future<T>> invokeAll(
					Collection<? extends Callable<T>> tasks, long timeout,
					TimeUnit unit) throws InterruptedException {
				throw new UnsupportedOperationException();
			}

			@Override
			public <T> T invokeAny(Collection<? extends Callable<T>> tasks)
					throws InterruptedException, ExecutionException {
				throw new UnsupportedOperationException();
			}

			@Override
			public <T> T invokeAny(Collection<? extends Callable<T>> tasks,
					long timeout, TimeUnit unit) throws InterruptedException,
					ExecutionException, TimeoutException {
				throw new UnsupportedOperationException();
			}
		};
	}

	/**
	 * Wraps an {@link Runnable} into an {@link Task}
	 * 
	 * @param command
	 *            the {@link Runnable} to be wrapped
	 * @return the wrapped {@link Runnable}
	 */
	public static Task<Runnable> wrap(final Runnable command) {
		return new Task<Runnable>() {
			@Override
			public Runnable call() throws Exception {
				command.run();
				return command;
			}
		};
	}

	/**
	 * Wraps an {@link Runnable} into an {@link Task}
	 * 
	 * @param <T>
	 *            the {@link Task} return type
	 * @param command
	 *            the {@link Runnable} to be wrapped
	 * @return the wrapped {@link Runnable}
	 */
	public static <T> Task<T> wrap(final Callable<T> command) {
		return new Task<T>() {
			@Override
			public T call() throws Exception {
				return command.call();
			}
		};
	}
}
