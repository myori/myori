package com.myori.core.utils.factory;

import java.lang.ref.ReferenceQueue;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Factory class to create {@link Collection} instances.
 */
public class CollectionFactory {
	/**
	 * Creates a new list of type <tt>T</tt>
	 * 
	 * @param <T>
	 *            the type
	 * @return the created list
	 */
	public static final <T> List<T> newList() {
		return new ArrayList<>();
	}

	/**
	 * Creates a new set of type <tt>T</tt>
	 * 
	 * @param <T>
	 *            the type
	 * @return the created set
	 */
	public static final <T> Set<T> newSet() {
		return new HashSet<>();
	}

	/**
	 * Creates a new concurrent queue of type <tt>T</tt>
	 * 
	 * @param <T>
	 *            the type
	 * @return the created queue
	 */
	public static final <T> Queue<T> newConcurrentQueue() {
		return new ConcurrentLinkedQueue<>();
	}

	/**
	 * Creates a new priority queue of type <tt>T</tt>
	 * 
	 * @param <T>
	 *            the type
	 * @return the created queue
	 */
	public static final <T> PriorityQueue<T> newPriorityQueue() {
		return new PriorityQueue<>();
	}

	/**
	 * Creates a new reference queue of type <tt>T</tt>
	 * 
	 * @param <T>
	 *            the type of the {@link ReferenceQueue}
	 * 
	 * @return the created queue
	 */
	public static final <T> ReferenceQueue<T> newReferenceQueue() {
		return new ReferenceQueue<>();
	}

	/**
	 * Creates a new map.
	 * 
	 * @param <K>
	 *            the key type
	 * @param <V>
	 *            the value type
	 * @return the new map
	 */
	public static final <K, V> Map<K, V> newMap() {
		return new HashMap<>();
	}

	/**
	 * Creates a new weak map.
	 * 
	 * @param <K>
	 *            the key type
	 * @param <V>
	 *            the value type
	 * @return the new map
	 */
	public static final <K, V> Map<K, V> newWeakMap() {
		return new WeakHashMap<>();
	}
}
