package com.myori.core.utils.exception;

/**
 * Base exception for Lineage 2
 */
public abstract class MyOriException extends Exception {
	/**
	 * Default Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see Exception#Exception()
	 */
	public MyOriException() {
		super();
	}

	/**
	 * @see Exception#Exception(String, Throwable)
	 */
	public MyOriException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @see Exception#Exception(String)
	 */
	public MyOriException(String message) {
		super(message);
	}

	/**
	 * @see Exception#Exception(Throwable)
	 */
	public MyOriException(Throwable cause) {
		super(cause);
	}

	// /**
	// * Each {@link MyOriException} has an {@link SystemMessage} attacked to it.
	// It
	// * is an <b><u>recommendation</u></b> of which message should be sent to
	// the
	// * client in case the exception is thrown.
	// *
	// * @return the recommended system message
	// */
	// public abstract SystemMessage getSystemMessage();
}
