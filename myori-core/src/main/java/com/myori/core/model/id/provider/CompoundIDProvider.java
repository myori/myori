package com.myori.core.model.id.provider;

import com.myori.core.model.id.ID;
import com.myori.core.model.id.compound.AbstractCompoundID;

/**
 * The ID factory is used to create instances of IDs. It will automatically make
 * sure the ID is free before allocating it.
 * 
 * @param <I1>
 *            the first compound {@link ID} type
 * @param <I2>
 *            the second compound {@link ID} type
 * @param <T>
 *            the {@link CompoundIDProvider} type
 */
public interface CompoundIDProvider<I1 extends ID<?>, I2 extends ID<?>, T extends AbstractCompoundID<I1, I2>> {
	/**
	 * Creates the ID object for an <b>EXISTING</b> ID.
	 * 
	 * @param id1
	 *            the first id
	 * @param id2
	 *            the second id
	 * @return the created compound {@link ID}
	 */
	T createID(I1 id1, I2 id2);

}
