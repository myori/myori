package com.myori.core.model;

import com.myori.core.model.id.ID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple model interface implementing {@link ID} related methods
 * 
 * @param <T>
 *            the ID type used to represent this {@link Model}
 * 
 * @author <a href="http://www.rogiel.com">Rogiel</a>
 */
public abstract class AbstractModel<T extends ID<?>> implements Model<T> {
	/**
	 * The logger instance
	 */
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * The object id
	 */
	protected T id;
	/**
	 * The database object state
	 */
	protected transient ObjectDesire desire = ObjectDesire.INSERT;

	@Override
	public T getID() {
		return id;
	}

	@Override
	public void setID(T ID) {
		desireInsert();
		this.id = ID;
	}

	@Override
	public ObjectDesire getObjectDesire() {
		return desire;
	}

	@Override
	public void setObjectDesire(ObjectDesire desire) {
		if (desire == null)
			desire = ObjectDesire.NONE;
		log.debug("{} set desire to {}", this, desire);
		this.desire = desire;
	}

	/**
	 * Set this object desire to {@link Model.ObjectDesire#UPDATE}. If the
	 * desire is {@link Model.ObjectDesire#INSERT} or
	 * {@link Model.ObjectDesire#DELETE} the desire will not be changed.
	 */
	protected void desireUpdate() {
		if (this.desire != ObjectDesire.INSERT
				&& this.desire != ObjectDesire.DELETE
				&& this.desire != ObjectDesire.TRANSIENT) {
			log.debug("{} desires an update", this);
			this.desire = ObjectDesire.UPDATE;
		}
	}

	/**
	 * Set this object desire to {@link Model.ObjectDesire#INSERT}. If the
	 * desire is {@link Model.ObjectDesire#DELETE} the desire will not be
	 * changed.
	 */
	protected void desireInsert() {
		if (this.desire != ObjectDesire.DELETE
				&& this.desire != ObjectDesire.TRANSIENT) {
			log.debug("{} desires an insert", this);
			this.desire = ObjectDesire.INSERT;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractModel<?> other = (AbstractModel<?>) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
