package com.myori.core.model.id;

import com.myori.core.model.Model;

/**
 * This is an abstract ID for most model objects.
 * 
 * @param <T>
 *            the raw id type
 * @param <O>
 *            the model this {@link ID} provides
 * 
 * @author <a href="http://www.rogiel.com">Rogiel</a>
 */
public abstract class AbstractModelID<T, O extends Model<? extends ID<T>>>
		extends ID<T> {
	/**
	 * @param id
	 *            the id
	 */
	protected AbstractModelID(T id) {
		super(id);
	}

	/**
	 * @return the {@link Model} object associated with this
	 *         {@link AbstractModelID}. <tt>null</tt> if {@link Model} does not
	 *         exists.
	 */
	public abstract O getObject();
}
