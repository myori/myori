package com.myori.core.model.id.compound;

import com.myori.core.model.id.ID;

/**
 * The compound {@link ID} is composed of two IDs.
 * 
 * @param <T1>
 *            the first {@link ID} type
 * @param <T2>
 *            the second {@link ID} type
 * 
 * @author <a href="http://www.rogiel.com">Rogiel</a>
 */
public class AbstractCompoundID<T1, T2> extends ID<AbstractCompoundID<T1, T2>> {
	/**
	 * The first ID
	 */
	private final T1 id1;
	/**
	 * The second ID
	 */
	private final T2 id2;

	/**
	 * Creates a new compound ID
	 * 
	 * @param id1
	 *            the first id
	 * @param id2
	 *            the second id
	 */
	protected AbstractCompoundID(T1 id1, T2 id2) {
		super(null);
		this.id1 = id1;
		this.id2 = id2;
	}

	/**
	 * @return the first id
	 */
	public T1 getID1() {
		return id1;
	}

	/**
	 * @return the second id
	 */
	public T2 getID2() {
		return id2;
	}

	@Override
	public AbstractCompoundID<T1, T2> getID() {
		return this;
	}
}
