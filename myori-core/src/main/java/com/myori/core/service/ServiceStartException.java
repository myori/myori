package com.myori.core.service;

/**
 * Exception thrown when a service failed to start.
 *
 */
public class ServiceStartException extends ServiceException {
    /**
     * The Java Serialization API serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of this exception
     */
    public ServiceStartException() {
        super();
    }

    /**
     * Creates a new instance of this exception
     *
     * @param message the message
     * @param cause   the root cause
     */
    public ServiceStartException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a new instance of this exception
     *
     * @param message the message
     */
    public ServiceStartException(String message) {
        super(message);
    }

    /**
     * Creates a new instance of this exception
     *
     * @param cause the root cause
     */
    public ServiceStartException(Throwable cause) {
        super(cause);
    }
}
