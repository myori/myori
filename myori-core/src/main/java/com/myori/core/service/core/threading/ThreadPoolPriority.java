package com.myori.core.service.core.threading;

/**
 * The priority of the thread pool
 */
public enum ThreadPoolPriority {
    /**
     * High priority.
     * <p>
     * Processor will block {@link ThreadPoolPriority#NORMAL} and
     * {@link ThreadPoolPriority#LOW} priority threads in order to finish tasks
     * in pools on this priority.
     */
    HIGH(Thread.MAX_PRIORITY),
    /**
     * Normal priority.
     * <p>
     * Processor will block {@link ThreadPoolPriority#LOW} priority threads in
     * order to finish tasks in pools on this priority.
     */
    NORMAL(Thread.NORM_PRIORITY),
    /**
     * Low priority.
     * <p>
     * Processor will give very low priority for tasks in this level.
     */
    LOW(Thread.MIN_PRIORITY);

    /**
     * The priority to be used on {@link Thread}
     */
    public final int threadPriority;

    /**
     * @param threadPriority the {@link Thread} priority {@link Integer} index
     */
    ThreadPoolPriority(int threadPriority) {
        this.threadPriority = threadPriority;
    }
}
