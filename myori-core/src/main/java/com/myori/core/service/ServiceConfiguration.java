package com.myori.core.service;

import com.myori.core.service.configuration.Configuration;

/**
 * Base interface for all {@link Service} {@link Configuration} classes.
 *
 * @author <a href="http://www.rogiel.com">Rogiel</a>
 */
public interface ServiceConfiguration extends Configuration {

}
