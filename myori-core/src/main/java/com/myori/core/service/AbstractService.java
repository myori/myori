package com.myori.core.service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An abstract service implementing basic life-cycle methods.
 *
 */
public abstract class AbstractService implements Service {
    /**
     * Running state of a service
     */
    private boolean running = false;

    @Override
    public final void start() throws ServiceStartException {
        if (running)
            throw new ServiceStartException("Service is already started");
        try {
            this.doStart();
            this.running = true;
        } catch (ServiceStartException e) {
            this.running = false;
            throw e;
        }
    }

    /**
     * Starts the service. This method is invoked internally by {@link #start()}
     *
     * @throws ServiceStartException if any error occur while starting the service
     */
    protected void doStart() throws ServiceStartException {
    }

    @Override
    public final void stop() throws ServiceStopException {
        if (!running)
            throw new ServiceStopException("Service is not started");
        try {
            this.doStop();
        } finally {
            this.running = false;
        }
    }

    /**
     * Stops the service. This method is invoked internally by {@link #stop()}
     *
     * @throws ServiceStopException if any error occur while stopping the service
     */
    protected void doStop() throws ServiceStopException {
    }

    @Override
    public void restart() throws ServiceException {
        this.stop();
        this.start();
    }

    @Override
    public boolean isStarted() {
        return running;
    }

    @Override
    public boolean isStopped() {
        return !running;
    }

    @Override
    public final Class<? extends Service>[] getDependencies() {
        final Depends deps = this.getClass().getAnnotation(Depends.class);
        if (deps == null)
            return null;
        return deps.value();
    }

    /**
     * Service dependency metadata
     *
     * @author <a href="http://www.rogiel.com">Rogiel</a>
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface Depends {
        /**
         * @return the service's dependency
         */
        Class<? extends Service>[] value();
    }
}
