package com.myori.core.service;

/**
 * Implements basic management for configurable services
 *
 * @param <T> the service configuration type
 */
public class AbstractConfigurableService<T extends ServiceConfiguration>
        extends AbstractService implements ConfigurableService<T> {
    /**
     * The service configuration
     */
    protected T config;
    /**
     * The service configuration class
     */
    private final Class<T> configType;

    /**
     * @param configType the service configuration class
     */
    public AbstractConfigurableService(Class<T> configType) {
        this.configType = configType;
    }

    /**
     * Transparently implements
     * {@link ConfigurableService#getConfigurationInterface()} without
     * implementing the interface
     *
     * @return the configuration interface set at {@link Configurable}
     * annotation, if present.
     */
    @Override
    public Class<T> getConfigurationInterface() {
        return configType;
    }

    @Override
    public void setConfiguration(T configuration) {
        config = configuration;
    }
}
