package com.myori.core.service.core.logging;

import com.myori.core.service.Service;

/**
 * This service initializes and configures the logging provider. Currently SLF4J
 * is used and implementations must initialize SLF4J and its respective binding.
 *
 */
public interface LoggingService extends Service {
}
