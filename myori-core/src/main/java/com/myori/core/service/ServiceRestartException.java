package com.myori.core.service;

/**
 * Thrown when an service failed to restart. It's <tt>cause</tt> can be an
 * {@link ServiceStartException} or {@link ServiceStopException}.
 *
 * @author <a href="http://www.rogiel.com">Rogiel</a>
 */
public class ServiceRestartException extends ServiceException {
    /**
     * The Java Serialization API serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of this exception
     */
    public ServiceRestartException() {
        super();
    }

    /**
     * Creates a new instance of this exception
     *
     * @param message the message
     * @param cause   the root cause
     */
    public ServiceRestartException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a new instance of this exception
     *
     * @param message the message
     */
    public ServiceRestartException(String message) {
        super(message);
    }

    /**
     * Creates a new instance of this exception
     *
     * @param cause the root cause
     */
    public ServiceRestartException(Throwable cause) {
        super(cause);
    }
}
