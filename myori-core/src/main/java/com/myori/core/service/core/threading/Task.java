package com.myori.core.service.core.threading;

import java.util.concurrent.Callable;

/**
 * @param <T> the task return type
 */
public interface Task<T> extends Callable<T> {
}
