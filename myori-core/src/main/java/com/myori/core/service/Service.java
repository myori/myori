package com.myori.core.service;

/**
 * Each Service is a provider of a given feature. Most services will want to
 * implement {@link AbstractService} class instead of this interface.
 *
 */
public interface Service {
    /**
     * Start this service
     *
     * @throws ServiceStartException if an error occurred
     */
    void start() throws ServiceStartException;

    /**
     * Stop this service
     *
     * @throws ServiceStopException if an error occurred
     */
    void stop() throws ServiceStopException;

    /**
     * Stop this service
     *
     * @throws ServiceException if an error occurred
     */
    void restart() throws ServiceException;

    /**
     * @return true if service is running
     */
    boolean isStarted();

    /**
     * @return false if service is not running
     */
    boolean isStopped();

    /**
     * @return the other services that the service depends on
     */
    Class<? extends Service>[] getDependencies();
}
