package com.myori.core.service.core;


import com.myori.core.service.Service;

/**
 * Service used to manage unhandled exceptions
 *
 */
public interface ExceptionService extends Service {
    /**
     * Handles an unhandled exception
     *
     * @param t the exception
     */
    void thrown(Throwable t);

    /**
     * Handles an unhandled exception and rethrows it again
     *
     * @param <T> the exception type
     * @param t   the exception
     * @throws T same exception in <tt>t</tt>
     */
    <T extends Throwable> void rethrown(T t) throws T;
}
