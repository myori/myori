package com.myori.core.service.core.threading;

import java.util.concurrent.ScheduledFuture;

/**
 * This future instance extends {@link ScheduledFuture}. An scheduled future
 * cannot return values neither await for its termination because its execution
 * is continuously repeated.
 */
public interface ScheduledAsyncFuture extends ScheduledFuture<Object> {
}
