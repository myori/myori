package com.myori.core.service.core.threading;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * This future instance extends {@link Future} but also adds some additional
 * features, such as waiting for an given task to finish.
 *
 * @param <T> the {@link Future} return type
 */
public interface AsyncFuture<T> extends Future<T> {
    /**
     * Waits until the task is executed
     *
     * @throws ExecutionException if the thread has been interrupted while waiting
     */
    void await() throws ExecutionException;

    /**
     * Waits until the task is executed
     *
     * @param timeout the timeout
     * @param unit    the timeout unit
     * @throws InterruptedException if the thread has been interrupted while waiting
     * @throws TimeoutException     if timeout was exceeded
     */
    void await(long timeout, TimeUnit unit) throws InterruptedException,
            TimeoutException;

    /**
     * Waits until the task is executed
     *
     * @return true if execution ended with no error, false otherwise
     */
    boolean awaitUninterruptibly();

    /**
     * Waits until the task is executed
     *
     * @param timeout the timeout
     * @param unit    the timeout unit
     * @return true if execution ended with no error, false otherwise. Please
     * note that false will be returned if the timeout has expired too!
     */
    boolean awaitUninterruptibly(long timeout, TimeUnit unit);

    /**
     * Adds an listener that will be notified once the executing has been
     * completed.
     *
     * @param listener the listener to be added
     */
    void addListener(AsyncListener<T> listener);

    /**
     * Removes an listener
     *
     * @param listener the listener to be removed
     */
    void removeListener(AsyncListener<T> listener);
}
