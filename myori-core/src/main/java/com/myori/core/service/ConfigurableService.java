package com.myori.core.service;

/**
 * Marks whether an service can be configured or not
 *
 * @param <T> the configuration interface type
 * @author <a href="http://www.rogiel.com">Rogiel</a>
 */
public interface ConfigurableService<T extends ServiceConfiguration> extends
        Service {
    /**
     * @return the configuration interface used by this service
     */
    Class<T> getConfigurationInterface();

    /**
     * Please note that this method will only be set at {@link Service#start()}.
     * Once {@link Service#stop()} is called, the configuration will be set to
     * <code>null</code>.
     *
     * @param configuration the service configuration instance
     */
    void setConfiguration(T configuration);
}
