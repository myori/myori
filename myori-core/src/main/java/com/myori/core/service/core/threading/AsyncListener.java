package com.myori.core.service.core.threading;

/**
 * @param <T> the return type of the {@link AsyncFuture}
 */
public interface AsyncListener<T> {
    /**
     * Called once the {@link AsyncFuture} has finished executing the task
     *
     * @param future the future
     * @param object the object returned. If any.
     */
    void onComplete(AsyncFuture<T> future, T object);
}
