package com.myori.core.service;

import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;


public class ServiceDescriptor<T extends Service> {
    private final Class<T> serviceInterface;
    private final Class<? extends T> serviceImplementation;
    private final Node node;

    /**
     * @param serviceInterface      the service interface
     * @param serviceImplementation the service implementation
     * @param node                  the XML node
     */
    public ServiceDescriptor(Class<T> serviceInterface, Class<? extends T> serviceImplementation, Node node) {
        this.serviceInterface = serviceInterface;
        this.serviceImplementation = serviceImplementation;
        this.node = node;
    }

    public Class<T> getServiceInterface() {
        return serviceInterface;
    }

    public Class<? extends T> getServiceImplementation() {
        return serviceImplementation;
    }

    public Node getNode() {
        return node;
    }

    public static ServiceDescriptor fromNode(Node node) throws ClassNotFoundException, DOMException {
        final NamedNodeMap attrs = node.getAttributes();
        final Class<? extends Service> serviceInterface = (Class<? extends Service>) Class
                .forName(attrs.getNamedItem("interface").getNodeValue());
        final Class<? extends Service> serviceImplementation = (Class<? extends Service>) Class
                .forName(attrs.getNamedItem("implementation").getNodeValue());
        return new ServiceDescriptor(serviceInterface, serviceImplementation, node);
    }
}
