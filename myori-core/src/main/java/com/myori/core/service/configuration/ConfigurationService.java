package com.myori.core.service.configuration;

import com.myori.core.service.ConfigurableService;
import com.myori.core.service.Service;
import com.myori.core.service.ServiceConfiguration;

/**
 * The configuration service is responsible for reading and writing in
 * configuration storage. Each configuration is represented by an interface.
 * Implementations of this interface are implementaion specific.
 */
public interface ConfigurationService extends Service {
    /**
     * Get the configuration for the given service
     *
     * @param <C>              the service configuration instance type
     * @param service          the service
     * @param serviceInterface the service interface
     * @return the configuration object
     */
    <C extends ServiceConfiguration> C getServiceConfiguration(ConfigurableService<?> service, Class<? extends Service> serviceInterface);
}
