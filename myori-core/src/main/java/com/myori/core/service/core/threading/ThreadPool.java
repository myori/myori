package com.myori.core.service.core.threading;

import java.util.concurrent.TimeUnit;

/**
 * This is an ThreadPool that you can use to asynchronously execute tasks.
 */
public interface ThreadPool {
    /**
     * Executes an asynchronous tasks.
     *
     * @param <T>  the task return type
     * @param task the callable instance
     * @return the {@link AsyncFuture} notified once the task has completed
     */
    <T> AsyncFuture<T> async(Task<T> task);

    /**
     * Executes an asynchronous tasks at an scheduled time.
     *
     * @param <T>   the task return type
     * @param task  the callable instance
     * @param delay the initial delay to wait before the task is executed
     * @param unit  the time unit of delay
     * @return the {@link AsyncFuture} notified once the task has completed
     */
    <T> AsyncFuture<T> async(long delay, TimeUnit unit, Task<T> task);

    /**
     * Executes an asynchronous tasks at an scheduled time.
     *
     * @param delay  the initial delay to wait before the task is executed
     * @param unit   the time unit of delay
     * @param repeat the repeating interval for this task
     * @param task   the task to be executed
     * @return the {@link AsyncFuture} notified once the task has completed
     */
    ScheduledAsyncFuture async(long delay, TimeUnit unit, long repeat,
                               Runnable task);

    /**
     * Disposes this thread pool. After disposing, it will no longer be able to
     * execute tasks.
     */
    void dispose();

    /**
     * @return true if the thread pool is no longer usable (i.e. was disposed)
     */
    boolean isDisposed();
}
