package com.myori.core.service.core.threading;

import com.myori.core.service.Service;

import java.util.concurrent.TimeUnit;

/**
 * This service is responsible for scheduling tasks and executing them in
 * parallel.
 */
public interface ThreadService extends Service {
    /**
     * Executes an asynchronous tasks.
     * <p>
     * Tasks scheduled here will go to an default shared thread pool.
     *
     * @param <T>      the task return type
     * @param callable the callable instance
     * @return the {@link AsyncFuture} notified once the task has completed
     */
    <T> AsyncFuture<T> async(Task<T> callable);

    /**
     * Executes an asynchronous tasks at an scheduled time. <b>Please note that
     * resources in scheduled thread pool are limited and tasks should be
     * performed fast.</b>
     * <p>
     * Tasks scheduled here will go to an default shared thread pool.
     *
     * @param <T>      the task return type
     * @param callable the callable instance
     * @param delay    the initial delay to wait before the task is executed
     * @param unit     the time unit of delay
     * @return the {@link AsyncFuture} notified once the task has completed
     */
    <T> AsyncFuture<T> async(long delay, TimeUnit unit, Task<T> callable);

    /**
     * Executes an asynchronous tasks at an scheduled time. <b>Please note that
     * resources in scheduled thread pool are limited and tasks should be
     * performed fast.</b>
     * <p>
     * Tasks scheduled here will go to an default shared thread pool.
     *
     * @param delay  the initial delay to wait before the task is executed
     * @param unit   the time unit of delay
     * @param repeat the repeating interval for this task
     * @param task   the task to be executed
     * @return the {@link AsyncFuture} notified once the task has completed
     */
    ScheduledAsyncFuture async(long delay, TimeUnit unit, long repeat,
                               Runnable task);

    /**
     * Creates a new thread pool with {@link ThreadPoolPriority#NORMAL normal}
     * priority and that can be increased up to {@link Integer#MAX_VALUE} active
     * threads, if necessary.
     * <p>
     * Threads in this pool will never expire.
     *
     * @param name    the pool name
     * @param threads the maximum amount of active threads
     * @return the new thread pool
     */
    ThreadPool createThreadPool(String name, int threads);

    /**
     * Creates a new thread pool that can increase up to
     * {@link Integer#MAX_VALUE} active threads, if necessary.
     * <p>
     * Threads in this pool will never expire.
     *
     * @param name     the pool name
     * @param threads  the maximum amount of active threads
     * @param priority the processor scheduling priority
     * @return the new thread pool
     */
    ThreadPool createThreadPool(String name, int threads,
                                ThreadPoolPriority priority);

    /**
     * Creates a new thread pool with {@link ThreadPoolPriority#NORMAL normal}
     * priority.
     *
     * @param name              the pool name
     * @param threads           the maximum amount of active threads
     * @param threadTimeout     the time it takes to expire an inactive thread
     * @param threadTimeoutUnit the {@link TimeUnit} for <code>threadTimeout</code>
     * @return the new thread pool
     */
    ThreadPool createThreadPool(String name, int threads, long threadTimeout,
                                TimeUnit threadTimeoutUnit);

    /**
     * Creates a new thread pool.
     *
     * @param name              the pool name
     * @param threads           the maximum amount of active threads
     * @param threadTimeout     the time it takes to expire an inactive thread
     * @param threadTimeoutUnit the {@link TimeUnit} for <code>threadTimeout</code>
     * @param priority          the processor scheduling priority
     * @return the new thread pool
     */
    ThreadPool createThreadPool(String name, int threads, long threadTimeout,
                                TimeUnit threadTimeoutUnit, ThreadPoolPriority priority);

    /**
     * Disposes an given thread pool. After disposing the thread pool will no
     * longer be usable.
     *
     * @param pool the thread pool to be disposed
     */
    void dispose(ThreadPool pool);
}
