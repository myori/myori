package com.myori.core.service.configuration;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Configuration interface
 * <p>
 * Each service desiring to use the configuration system must extend the
 * interface and define methods for getters and setters. Each method must be
 * annotated either with {@link ConfigurationPropertyGetter} or
 * {@link ConfigurationPropertySetter}.
 */
public interface Configuration {
    /**
     * The getter method for an configuration property
     * <p>
     * The method must have no arguments.
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(value = ElementType.METHOD)
    @Documented
    @interface ConfigurationPropertyGetter {
        /**
         * @return the default value to be used
         */
        String defaultValue() default "";
    }

    /**
     * The setter method for an configuration property.
     * <p>
     * The method must have a single argument and return type void.
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(value = ElementType.METHOD)
    @Documented
    @interface ConfigurationPropertySetter {
    }
}
