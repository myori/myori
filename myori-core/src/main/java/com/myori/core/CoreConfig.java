package com.myori.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.myori.core")
public class CoreConfig {

    private final Logger LOGGER = LoggerFactory.getLogger(CoreConfig.class);

    public CoreConfig() {
        LOGGER.info("Create MyOri-Core context configuration");
    }

}
