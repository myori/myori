package com.myori.core.module;

public interface Module {

    ModuleDetails getDetails();

}
